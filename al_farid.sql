-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2019 at 08:43 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `al_farid`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `order`, `view_count`, `created_at`, `updated_at`) VALUES
(3, 'الاسماك', 'الاسماك', 1, 0, '2019-10-20 10:43:05', '2019-11-18 13:39:36'),
(7, 'المواشي', 'المواشي', 2, 0, '2019-11-18 13:39:19', '2019-11-18 13:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `ar` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `ar`, `en`, `provinceId`) VALUES
(1, 194, 'تبوك', 'Tabuk', 3),
(3, 194, 'الرياض', 'Riyadh', 6),
(5, 194, 'الطائف', 'At Taif', 11),
(6, 194, 'مكة المكرمة', 'Makkah Al Mukarramah', 11),
(10, 194, 'حائل', 'Hail', 4),
(11, 194, 'بريدة', 'Buraydah', 5),
(12, 194, 'الهفوف', 'Al Hufuf', 13),
(13, 194, 'الدمام', 'Ad Dammam', 13),
(14, 194, 'المدينة المنورة', 'Al Madinah Al Munaww', 7),
(15, 194, 'ابها', 'Abha', 8),
(17, 194, 'جازان', 'Jazan', 10),
(18, 194, 'جدة', 'Jeddah', 11),
(24, 194, 'المجمعة', 'Al Majmaah', 6),
(31, 194, 'الخبر', 'Al Khubar', 13),
(47, 194, 'حفر الباطن', 'Hafar Al Batin', 13),
(62, 194, 'خميس مشيط', 'Khamis Mushayt', 8),
(65, 194, 'احد رفيده', 'Ahad Rifaydah', 8),
(67, 194, 'القطيف', 'Al Qatif', 13),
(80, 194, 'عنيزة', 'Unayzah', 5),
(89, 194, 'قرية العليا', 'Qaryat Al Ulya', 13),
(113, 194, 'الجبيل', 'Al Jubail', 13),
(115, 194, 'النعيرية', 'An Nuayriyah', 13),
(227, 194, 'الظهران', 'Dhahran', 13),
(233, 194, 'الوجه', 'Al Wajh', 3),
(243, 194, 'بقيق', 'Buqayq', 13),
(270, 194, 'الزلفي', 'Az Zulfi', 6),
(288, 194, 'خيبر', 'Khaybar', 7),
(306, 194, 'الغاط', 'Al Ghat', 6),
(323, 194, 'املج', 'Umluj', 3),
(377, 194, 'رابغ', 'Rabigh', 11),
(418, 194, 'عفيف', 'Afif', 6),
(443, 194, 'ثادق', 'Thadiq', 6),
(454, 194, 'سيهات', 'Sayhat', 13),
(456, 194, 'تاروت', 'Tarut', 13),
(483, 194, 'ينبع', 'Yanbu', 7),
(500, 194, 'شقراء', 'Shaqra', 6),
(669, 194, 'الدوادمي', 'Ad Duwadimi', 6),
(828, 194, 'الدرعية', 'Ad Diriyah', 6),
(880, 194, 'القويعية', 'Quwayiyah', 6),
(990, 194, 'المزاحمية', 'Al Muzahimiyah', 6),
(1053, 194, 'بدر', 'Badr', 7),
(1061, 194, 'الخرج', 'Al Kharj', 6),
(1073, 194, 'الدلم', 'Ad Dilam', 6),
(1228, 194, 'الشنان', 'Ash Shinan', 4),
(1248, 194, 'الخرمة', 'Al Khurmah', 11),
(1257, 194, 'الجموم', 'Al Jumum', 11),
(1294, 194, 'المجاردة', 'Al Majardah', 8),
(1361, 194, 'السليل', 'As Sulayyil', 6),
(1443, 194, 'تثليث', 'Tathilith', 8),
(1514, 194, 'بيشة', 'Bishah', 8),
(1542, 194, 'الباحة', 'Al Baha', 9),
(1625, 194, 'القنفذة', 'Al Qunfidhah', 11),
(1801, 194, 'محايل', 'Muhayil', 8),
(1879, 194, 'ثول', 'Thuwal', 11),
(1947, 194, 'ضبا', 'Duba', 3),
(2156, 194, 'تربه', 'Turbah', 11),
(2167, 194, 'صفوى', 'Safwa', 13),
(2171, 194, 'عنك', 'Inak', 13),
(2208, 194, 'طريف', 'Turaif', 1),
(2213, 194, 'عرعر', 'Arar', 1),
(2226, 194, 'القريات', 'Al Qurayyat', 2),
(2237, 194, 'سكاكا', 'Sakaka', 2),
(2256, 194, 'رفحاء', 'Rafha', 1),
(2268, 194, 'دومة الجندل', 'Dawmat Al Jandal', 2),
(2421, 194, 'الرس', 'Ar Rass', 5),
(2448, 194, 'المذنب', 'Al Midhnab', 5),
(2464, 194, 'الخفجي', 'Al Khafji', 13),
(2467, 194, 'رياض الخبراء', 'Riyad Al Khabra', 5),
(2481, 194, 'البدائع', 'Al Badai', 5),
(2590, 194, 'رأس تنورة', 'Ras Tannurah', 13),
(2630, 194, 'البكيرية', 'Al Bukayriyah', 5),
(2777, 194, 'الشماسية', 'Ash Shimasiyah', 5),
(3158, 194, 'الحريق', 'Al Hariq', 6),
(3161, 194, 'حوطة بني تميم', 'Hawtat Bani Tamim', 6),
(3174, 194, 'ليلى', 'Layla', 6),
(3275, 194, 'بللسمر', 'Billasmar', 8),
(3347, 194, 'شرورة', 'Sharurah', 12),
(3417, 194, 'نجران', 'Najran', 12),
(3479, 194, 'صبيا', 'Sabya', 10),
(3525, 194, 'ابو عريش', 'Abu Arish', 10),
(3542, 194, 'صامطة', 'Samtah', 10),
(3652, 194, 'احد المسارحة', 'Ahad Al Musarihah', 10),
(3666, 194, 'مدينة الملك عبدالله ', 'King Abdullah Econom', 11);

-- --------------------------------------------------------

--
-- Table structure for table `common_questions`
--

CREATE TABLE `common_questions` (
  `id` int(11) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `common_questions`
--

INSERT INTO `common_questions` (`id`, `question`, `answer`, `order`, `created_at`, `updated_at`) VALUES
(1, 'سؤال للتيست', '<p><strong>سؤاااااااااااااااااااااال</strong></p>', 2, '2019-10-22 16:10:56', '2019-11-18 12:40:40'),
(2, 'سسسسسؤال', '<p style=\"text-align: center;\"><span style=\"text-decoration: underline;\">سسسسسسسسسسسسسسسسسسسسسسسسؤال</span></p>', 4, '2019-10-22 16:11:14', '2019-11-18 12:40:40'),
(3, 'سؤال جديد', '<p><em><strong>سؤاااااااااااااااااااااال</strong></em></p>', 1, '2019-10-22 16:10:56', '2019-10-22 16:18:51'),
(4, 'ماهو هو هدف الموقع', '<p><strong>سؤاااااااااااااااااااااال</strong></p>', 3, '2019-10-22 16:10:56', '2019-11-18 12:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) UNSIGNED NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_en` varchar(100) NOT NULL DEFAULT '',
  `country_ar` varchar(100) NOT NULL DEFAULT '',
  `country_enNationality` varchar(100) NOT NULL DEFAULT '',
  `country_arNationality` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_en`, `country_ar`, `country_enNationality`, `country_arNationality`) VALUES
(1, 'AF', 'Afghanistan', 'أفغانستان', 'Afghan', 'أفغانستاني'),
(2, 'AL', 'Albania', 'ألبانيا', 'Albanian', 'ألباني'),
(3, 'AX', 'Aland Islands', 'جزر آلاند', 'Aland Islander', 'آلاندي'),
(4, 'DZ', 'Algeria', 'الجزائر', 'Algerian', 'جزائري'),
(5, 'AS', 'American Samoa', 'ساموا-الأمريكي', 'American Samoan', 'أمريكي سامواني'),
(6, 'AD', 'Andorra', 'أندورا', 'Andorran', 'أندوري'),
(7, 'AO', 'Angola', 'أنغولا', 'Angolan', 'أنقولي'),
(8, 'AI', 'Anguilla', 'أنغويلا', 'Anguillan', 'أنغويلي'),
(9, 'AQ', 'Antarctica', 'أنتاركتيكا', 'Antarctican', 'أنتاركتيكي'),
(10, 'AG', 'Antigua and Barbuda', 'أنتيغوا وبربودا', 'Antiguan', 'بربودي'),
(11, 'AR', 'Argentina', 'الأرجنتين', 'Argentinian', 'أرجنتيني'),
(12, 'AM', 'Armenia', 'أرمينيا', 'Armenian', 'أرميني'),
(13, 'AW', 'Aruba', 'أروبه', 'Aruban', 'أوروبهيني'),
(14, 'AU', 'Australia', 'أستراليا', 'Australian', 'أسترالي'),
(15, 'AT', 'Austria', 'النمسا', 'Austrian', 'نمساوي'),
(16, 'AZ', 'Azerbaijan', 'أذربيجان', 'Azerbaijani', 'أذربيجاني'),
(17, 'BS', 'Bahamas', 'الباهاماس', 'Bahamian', 'باهاميسي'),
(18, 'BH', 'Bahrain', 'البحرين', 'Bahraini', 'بحريني'),
(19, 'BD', 'Bangladesh', 'بنغلاديش', 'Bangladeshi', 'بنغلاديشي'),
(20, 'BB', 'Barbados', 'بربادوس', 'Barbadian', 'بربادوسي'),
(21, 'BY', 'Belarus', 'روسيا البيضاء', 'Belarusian', 'روسي'),
(22, 'BE', 'Belgium', 'بلجيكا', 'Belgian', 'بلجيكي'),
(23, 'BZ', 'Belize', 'بيليز', 'Belizean', 'بيليزي'),
(24, 'BJ', 'Benin', 'بنين', 'Beninese', 'بنيني'),
(25, 'BL', 'Saint Barthelemy', 'سان بارتيلمي', 'Saint Barthelmian', 'سان بارتيلمي'),
(26, 'BM', 'Bermuda', 'جزر برمودا', 'Bermudan', 'برمودي'),
(27, 'BT', 'Bhutan', 'بوتان', 'Bhutanese', 'بوتاني'),
(28, 'BO', 'Bolivia', 'بوليفيا', 'Bolivian', 'بوليفي'),
(29, 'BA', 'Bosnia and Herzegovina', 'البوسنة و الهرسك', 'Bosnian / Herzegovinian', 'بوسني/هرسكي'),
(30, 'BW', 'Botswana', 'بوتسوانا', 'Botswanan', 'بوتسواني'),
(31, 'BV', 'Bouvet Island', 'جزيرة بوفيه', 'Bouvetian', 'بوفيهي'),
(32, 'BR', 'Brazil', 'البرازيل', 'Brazilian', 'برازيلي'),
(33, 'IO', 'British Indian Ocean Territory', 'إقليم المحيط الهندي البريطاني', 'British Indian Ocean Territory', 'إقليم المحيط الهندي البريطاني'),
(34, 'BN', 'Brunei Darussalam', 'بروني', 'Bruneian', 'بروني'),
(35, 'BG', 'Bulgaria', 'بلغاريا', 'Bulgarian', 'بلغاري'),
(36, 'BF', 'Burkina Faso', 'بوركينا فاسو', 'Burkinabe', 'بوركيني'),
(37, 'BI', 'Burundi', 'بوروندي', 'Burundian', 'بورونيدي'),
(38, 'KH', 'Cambodia', 'كمبوديا', 'Cambodian', 'كمبودي'),
(39, 'CM', 'Cameroon', 'كاميرون', 'Cameroonian', 'كاميروني'),
(40, 'CA', 'Canada', 'كندا', 'Canadian', 'كندي'),
(41, 'CV', 'Cape Verde', 'الرأس الأخضر', 'Cape Verdean', 'الرأس الأخضر'),
(42, 'KY', 'Cayman Islands', 'جزر كايمان', 'Caymanian', 'كايماني'),
(43, 'CF', 'Central African Republic', 'جمهورية أفريقيا الوسطى', 'Central African', 'أفريقي'),
(44, 'TD', 'Chad', 'تشاد', 'Chadian', 'تشادي'),
(45, 'CL', 'Chile', 'شيلي', 'Chilean', 'شيلي'),
(46, 'CN', 'China', 'الصين', 'Chinese', 'صيني'),
(47, 'CX', 'Christmas Island', 'جزيرة عيد الميلاد', 'Christmas Islander', 'جزيرة عيد الميلاد'),
(48, 'CC', 'Cocos (Keeling) Islands', 'جزر كوكوس', 'Cocos Islander', 'جزر كوكوس'),
(49, 'CO', 'Colombia', 'كولومبيا', 'Colombian', 'كولومبي'),
(50, 'KM', 'Comoros', 'جزر القمر', 'Comorian', 'جزر القمر'),
(51, 'CG', 'Congo', 'الكونغو', 'Congolese', 'كونغي'),
(52, 'CK', 'Cook Islands', 'جزر كوك', 'Cook Islander', 'جزر كوك'),
(53, 'CR', 'Costa Rica', 'كوستاريكا', 'Costa Rican', 'كوستاريكي'),
(54, 'HR', 'Croatia', 'كرواتيا', 'Croatian', 'كوراتي'),
(55, 'CU', 'Cuba', 'كوبا', 'Cuban', 'كوبي'),
(56, 'CY', 'Cyprus', 'قبرص', 'Cypriot', 'قبرصي'),
(57, 'CW', 'Curaçao', 'كوراساو', 'Curacian', 'كوراساوي'),
(58, 'CZ', 'Czech Republic', 'الجمهورية التشيكية', 'Czech', 'تشيكي'),
(59, 'DK', 'Denmark', 'الدانمارك', 'Danish', 'دنماركي'),
(60, 'DJ', 'Djibouti', 'جيبوتي', 'Djiboutian', 'جيبوتي'),
(61, 'DM', 'Dominica', 'دومينيكا', 'Dominican', 'دومينيكي'),
(62, 'DO', 'Dominican Republic', 'الجمهورية الدومينيكية', 'Dominican', 'دومينيكي'),
(63, 'EC', 'Ecuador', 'إكوادور', 'Ecuadorian', 'إكوادوري'),
(64, 'EG', 'Egypt', 'مصر', 'Egyptian', 'مصري'),
(65, 'SV', 'El Salvador', 'إلسلفادور', 'Salvadoran', 'سلفادوري'),
(66, 'GQ', 'Equatorial Guinea', 'غينيا الاستوائي', 'Equatorial Guinean', 'غيني'),
(67, 'ER', 'Eritrea', 'إريتريا', 'Eritrean', 'إريتيري'),
(68, 'EE', 'Estonia', 'استونيا', 'Estonian', 'استوني'),
(69, 'ET', 'Ethiopia', 'أثيوبيا', 'Ethiopian', 'أثيوبي'),
(70, 'FK', 'Falkland Islands (Malvinas)', 'جزر فوكلاند', 'Falkland Islander', 'فوكلاندي'),
(71, 'FO', 'Faroe Islands', 'جزر فارو', 'Faroese', 'جزر فارو'),
(72, 'FJ', 'Fiji', 'فيجي', 'Fijian', 'فيجي'),
(73, 'FI', 'Finland', 'فنلندا', 'Finnish', 'فنلندي'),
(74, 'FR', 'France', 'فرنسا', 'French', 'فرنسي'),
(75, 'GF', 'French Guiana', 'غويانا الفرنسية', 'French Guianese', 'غويانا الفرنسية'),
(76, 'PF', 'French Polynesia', 'بولينيزيا الفرنسية', 'French Polynesian', 'بولينيزيي'),
(77, 'TF', 'French Southern and Antarctic Lands', 'أراض فرنسية جنوبية وأنتارتيكية', 'French', 'أراض فرنسية جنوبية وأنتارتيكية'),
(78, 'GA', 'Gabon', 'الغابون', 'Gabonese', 'غابوني'),
(79, 'GM', 'Gambia', 'غامبيا', 'Gambian', 'غامبي'),
(80, 'GE', 'Georgia', 'جيورجيا', 'Georgian', 'جيورجي'),
(81, 'DE', 'Germany', 'ألمانيا', 'German', 'ألماني'),
(82, 'GH', 'Ghana', 'غانا', 'Ghanaian', 'غاني'),
(83, 'GI', 'Gibraltar', 'جبل طارق', 'Gibraltar', 'جبل طارق'),
(84, 'GG', 'Guernsey', 'غيرنزي', 'Guernsian', 'غيرنزي'),
(85, 'GR', 'Greece', 'اليونان', 'Greek', 'يوناني'),
(86, 'GL', 'Greenland', 'جرينلاند', 'Greenlandic', 'جرينلاندي'),
(87, 'GD', 'Grenada', 'غرينادا', 'Grenadian', 'غرينادي'),
(88, 'GP', 'Guadeloupe', 'جزر جوادلوب', 'Guadeloupe', 'جزر جوادلوب'),
(89, 'GU', 'Guam', 'جوام', 'Guamanian', 'جوامي'),
(90, 'GT', 'Guatemala', 'غواتيمال', 'Guatemalan', 'غواتيمالي'),
(91, 'GN', 'Guinea', 'غينيا', 'Guinean', 'غيني'),
(92, 'GW', 'Guinea-Bissau', 'غينيا-بيساو', 'Guinea-Bissauan', 'غيني'),
(93, 'GY', 'Guyana', 'غيانا', 'Guyanese', 'غياني'),
(94, 'HT', 'Haiti', 'هايتي', 'Haitian', 'هايتي'),
(95, 'HM', 'Heard and Mc Donald Islands', 'جزيرة هيرد وجزر ماكدونالد', 'Heard and Mc Donald Islanders', 'جزيرة هيرد وجزر ماكدونالد'),
(96, 'HN', 'Honduras', 'هندوراس', 'Honduran', 'هندوراسي'),
(97, 'HK', 'Hong Kong', 'هونغ كونغ', 'Hongkongese', 'هونغ كونغي'),
(98, 'HU', 'Hungary', 'المجر', 'Hungarian', 'مجري'),
(99, 'IS', 'Iceland', 'آيسلندا', 'Icelandic', 'آيسلندي'),
(100, 'IN', 'India', 'الهند', 'Indian', 'هندي'),
(101, 'IM', 'Isle of Man', 'جزيرة مان', 'Manx', 'ماني'),
(102, 'ID', 'Indonesia', 'أندونيسيا', 'Indonesian', 'أندونيسيي'),
(103, 'IR', 'Iran', 'إيران', 'Iranian', 'إيراني'),
(104, 'IQ', 'Iraq', 'العراق', 'Iraqi', 'عراقي'),
(105, 'IE', 'Ireland', 'إيرلندا', 'Irish', 'إيرلندي'),
(106, 'IL', 'Israel', 'إسرائيل', 'Israeli', 'إسرائيلي'),
(107, 'IT', 'Italy', 'إيطاليا', 'Italian', 'إيطالي'),
(108, 'CI', 'Ivory Coast', 'ساحل العاج', 'Ivory Coastian', 'ساحل العاج'),
(109, 'JE', 'Jersey', 'جيرزي', 'Jersian', 'جيرزي'),
(110, 'JM', 'Jamaica', 'جمايكا', 'Jamaican', 'جمايكي'),
(111, 'JP', 'Japan', 'اليابان', 'Japanese', 'ياباني'),
(112, 'JO', 'Jordan', 'الأردن', 'Jordanian', 'أردني'),
(113, 'KZ', 'Kazakhstan', 'كازاخستان', 'Kazakh', 'كازاخستاني'),
(114, 'KE', 'Kenya', 'كينيا', 'Kenyan', 'كيني'),
(115, 'KI', 'Kiribati', 'كيريباتي', 'I-Kiribati', 'كيريباتي'),
(116, 'KP', 'Korea(North Korea)', 'كوريا الشمالية', 'North Korean', 'كوري'),
(117, 'KR', 'Korea(South Korea)', 'كوريا الجنوبية', 'South Korean', 'كوري'),
(118, 'XK', 'Kosovo', 'كوسوفو', 'Kosovar', 'كوسيفي'),
(119, 'KW', 'Kuwait', 'الكويت', 'Kuwaiti', 'كويتي'),
(120, 'KG', 'Kyrgyzstan', 'قيرغيزستان', 'Kyrgyzstani', 'قيرغيزستاني'),
(121, 'LA', 'Lao PDR', 'لاوس', 'Laotian', 'لاوسي'),
(122, 'LV', 'Latvia', 'لاتفيا', 'Latvian', 'لاتيفي'),
(123, 'LB', 'Lebanon', 'لبنان', 'Lebanese', 'لبناني'),
(124, 'LS', 'Lesotho', 'ليسوتو', 'Basotho', 'ليوسيتي'),
(125, 'LR', 'Liberia', 'ليبيريا', 'Liberian', 'ليبيري'),
(126, 'LY', 'Libya', 'ليبيا', 'Libyan', 'ليبي'),
(127, 'LI', 'Liechtenstein', 'ليختنشتين', 'Liechtenstein', 'ليختنشتيني'),
(128, 'LT', 'Lithuania', 'لتوانيا', 'Lithuanian', 'لتوانيي'),
(129, 'LU', 'Luxembourg', 'لوكسمبورغ', 'Luxembourger', 'لوكسمبورغي'),
(130, 'LK', 'Sri Lanka', 'سريلانكا', 'Sri Lankian', 'سريلانكي'),
(131, 'MO', 'Macau', 'ماكاو', 'Macanese', 'ماكاوي'),
(132, 'MK', 'Macedonia', 'مقدونيا', 'Macedonian', 'مقدوني'),
(133, 'MG', 'Madagascar', 'مدغشقر', 'Malagasy', 'مدغشقري'),
(134, 'MW', 'Malawi', 'مالاوي', 'Malawian', 'مالاوي'),
(135, 'MY', 'Malaysia', 'ماليزيا', 'Malaysian', 'ماليزي'),
(136, 'MV', 'Maldives', 'المالديف', 'Maldivian', 'مالديفي'),
(137, 'ML', 'Mali', 'مالي', 'Malian', 'مالي'),
(138, 'MT', 'Malta', 'مالطا', 'Maltese', 'مالطي'),
(139, 'MH', 'Marshall Islands', 'جزر مارشال', 'Marshallese', 'مارشالي'),
(140, 'MQ', 'Martinique', 'مارتينيك', 'Martiniquais', 'مارتينيكي'),
(141, 'MR', 'Mauritania', 'موريتانيا', 'Mauritanian', 'موريتانيي'),
(142, 'MU', 'Mauritius', 'موريشيوس', 'Mauritian', 'موريشيوسي'),
(143, 'YT', 'Mayotte', 'مايوت', 'Mahoran', 'مايوتي'),
(144, 'MX', 'Mexico', 'المكسيك', 'Mexican', 'مكسيكي'),
(145, 'FM', 'Micronesia', 'مايكرونيزيا', 'Micronesian', 'مايكرونيزيي'),
(146, 'MD', 'Moldova', 'مولدافيا', 'Moldovan', 'مولديفي'),
(147, 'MC', 'Monaco', 'موناكو', 'Monacan', 'مونيكي'),
(148, 'MN', 'Mongolia', 'منغوليا', 'Mongolian', 'منغولي'),
(149, 'ME', 'Montenegro', 'الجبل الأسود', 'Montenegrin', 'الجبل الأسود'),
(150, 'MS', 'Montserrat', 'مونتسيرات', 'Montserratian', 'مونتسيراتي'),
(151, 'MA', 'Morocco', 'المغرب', 'Moroccan', 'مغربي'),
(152, 'MZ', 'Mozambique', 'موزمبيق', 'Mozambican', 'موزمبيقي'),
(153, 'MM', 'Myanmar', 'ميانمار', 'Myanmarian', 'ميانماري'),
(154, 'NA', 'Namibia', 'ناميبيا', 'Namibian', 'ناميبي'),
(155, 'NR', 'Nauru', 'نورو', 'Nauruan', 'نوري'),
(156, 'NP', 'Nepal', 'نيبال', 'Nepalese', 'نيبالي'),
(157, 'NL', 'Netherlands', 'هولندا', 'Dutch', 'هولندي'),
(158, 'AN', 'Netherlands Antilles', 'جزر الأنتيل الهولندي', 'Dutch Antilier', 'هولندي'),
(159, 'NC', 'New Caledonia', 'كاليدونيا الجديدة', 'New Caledonian', 'كاليدوني'),
(160, 'NZ', 'New Zealand', 'نيوزيلندا', 'New Zealander', 'نيوزيلندي'),
(161, 'NI', 'Nicaragua', 'نيكاراجوا', 'Nicaraguan', 'نيكاراجوي'),
(162, 'NE', 'Niger', 'النيجر', 'Nigerien', 'نيجيري'),
(163, 'NG', 'Nigeria', 'نيجيريا', 'Nigerian', 'نيجيري'),
(164, 'NU', 'Niue', 'ني', 'Niuean', 'ني'),
(165, 'NF', 'Norfolk Island', 'جزيرة نورفولك', 'Norfolk Islander', 'نورفوليكي'),
(166, 'MP', 'Northern Mariana Islands', 'جزر ماريانا الشمالية', 'Northern Marianan', 'ماريني'),
(167, 'NO', 'Norway', 'النرويج', 'Norwegian', 'نرويجي'),
(168, 'OM', 'Oman', 'عمان', 'Omani', 'عماني'),
(169, 'PK', 'Pakistan', 'باكستان', 'Pakistani', 'باكستاني'),
(170, 'PW', 'Palau', 'بالاو', 'Palauan', 'بالاوي'),
(171, 'PS', 'Palestine', 'فلسطين', 'Palestinian', 'فلسطيني'),
(172, 'PA', 'Panama', 'بنما', 'Panamanian', 'بنمي'),
(173, 'PG', 'Papua New Guinea', 'بابوا غينيا الجديدة', 'Papua New Guinean', 'بابوي'),
(174, 'PY', 'Paraguay', 'باراغواي', 'Paraguayan', 'بارغاوي'),
(175, 'PE', 'Peru', 'بيرو', 'Peruvian', 'بيري'),
(176, 'PH', 'Philippines', 'الفليبين', 'Filipino', 'فلبيني'),
(177, 'PN', 'Pitcairn', 'بيتكيرن', 'Pitcairn Islander', 'بيتكيرني'),
(178, 'PL', 'Poland', 'بولونيا', 'Polish', 'بوليني'),
(179, 'PT', 'Portugal', 'البرتغال', 'Portuguese', 'برتغالي'),
(180, 'PR', 'Puerto Rico', 'بورتو ريكو', 'Puerto Rican', 'بورتي'),
(181, 'QA', 'Qatar', 'قطر', 'Qatari', 'قطري'),
(182, 'RE', 'Reunion Island', 'ريونيون', 'Reunionese', 'ريونيوني'),
(183, 'RO', 'Romania', 'رومانيا', 'Romanian', 'روماني'),
(184, 'RU', 'Russian', 'روسيا', 'Russian', 'روسي'),
(185, 'RW', 'Rwanda', 'رواندا', 'Rwandan', 'رواندا'),
(186, 'KN', 'Saint Kitts and Nevis', 'سانت كيتس ونيفس,', 'Kittitian/Nevisian', 'سانت كيتس ونيفس'),
(187, 'MF', 'Saint Martin (French part)', 'ساينت مارتن فرنسي', 'St. Martian(French)', 'ساينت مارتني فرنسي'),
(188, 'SX', 'Sint Maarten (Dutch part)', 'ساينت مارتن هولندي', 'St. Martian(Dutch)', 'ساينت مارتني هولندي'),
(189, 'LC', 'Saint Pierre and Miquelon', 'سان بيير وميكلون', 'St. Pierre and Miquelon', 'سان بيير وميكلوني'),
(190, 'VC', 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين'),
(191, 'WS', 'Samoa', 'ساموا', 'Samoan', 'ساموي'),
(192, 'SM', 'San Marino', 'سان مارينو', 'Sammarinese', 'ماريني'),
(193, 'ST', 'Sao Tome and Principe', 'ساو تومي وبرينسيبي', 'Sao Tomean', 'ساو تومي وبرينسيبي'),
(194, 'SA', 'Saudi Arabia', 'المملكة العربية السعودية', 'Saudi Arabian', 'سعودي'),
(195, 'SN', 'Senegal', 'السنغال', 'Senegalese', 'سنغالي'),
(196, 'RS', 'Serbia', 'صربيا', 'Serbian', 'صربي'),
(197, 'SC', 'Seychelles', 'سيشيل', 'Seychellois', 'سيشيلي'),
(198, 'SL', 'Sierra Leone', 'سيراليون', 'Sierra Leonean', 'سيراليوني'),
(199, 'SG', 'Singapore', 'سنغافورة', 'Singaporean', 'سنغافوري'),
(200, 'SK', 'Slovakia', 'سلوفاكيا', 'Slovak', 'سولفاكي'),
(201, 'SI', 'Slovenia', 'سلوفينيا', 'Slovenian', 'سولفيني'),
(202, 'SB', 'Solomon Islands', 'جزر سليمان', 'Solomon Island', 'جزر سليمان'),
(203, 'SO', 'Somalia', 'الصومال', 'Somali', 'صومالي'),
(204, 'ZA', 'South Africa', 'جنوب أفريقيا', 'South African', 'أفريقي'),
(205, 'GS', 'South Georgia and the South Sandwich', 'المنطقة القطبية الجنوبية', 'South Georgia and the South Sandwich', 'لمنطقة القطبية الجنوبية'),
(206, 'SS', 'South Sudan', 'السودان الجنوبي', 'South Sudanese', 'سوادني جنوبي'),
(207, 'ES', 'Spain', 'إسبانيا', 'Spanish', 'إسباني'),
(208, 'SH', 'Saint Helena', 'سانت هيلانة', 'St. Helenian', 'هيلاني'),
(209, 'SD', 'Sudan', 'السودان', 'Sudanese', 'سوداني'),
(210, 'SR', 'Suriname', 'سورينام', 'Surinamese', 'سورينامي'),
(211, 'SJ', 'Svalbard and Jan Mayen', 'سفالبارد ويان ماين', 'Svalbardian/Jan Mayenian', 'سفالبارد ويان ماين'),
(212, 'SZ', 'Swaziland', 'سوازيلند', 'Swazi', 'سوازيلندي'),
(213, 'SE', 'Sweden', 'السويد', 'Swedish', 'سويدي'),
(214, 'CH', 'Switzerland', 'سويسرا', 'Swiss', 'سويسري'),
(215, 'SY', 'Syria', 'سوريا', 'Syrian', 'سوري'),
(216, 'TW', 'Taiwan', 'تايوان', 'Taiwanese', 'تايواني'),
(217, 'TJ', 'Tajikistan', 'طاجيكستان', 'Tajikistani', 'طاجيكستاني'),
(218, 'TZ', 'Tanzania', 'تنزانيا', 'Tanzanian', 'تنزانيي'),
(219, 'TH', 'Thailand', 'تايلندا', 'Thai', 'تايلندي'),
(220, 'TL', 'Timor-Leste', 'تيمور الشرقية', 'Timor-Lestian', 'تيموري'),
(221, 'TG', 'Togo', 'توغو', 'Togolese', 'توغي'),
(222, 'TK', 'Tokelau', 'توكيلاو', 'Tokelaian', 'توكيلاوي'),
(223, 'TO', 'Tonga', 'تونغا', 'Tongan', 'تونغي'),
(224, 'TT', 'Trinidad and Tobago', 'ترينيداد وتوباغو', 'Trinidadian/Tobagonian', 'ترينيداد وتوباغو'),
(225, 'TN', 'Tunisia', 'تونس', 'Tunisian', 'تونسي'),
(226, 'TR', 'Turkey', 'تركيا', 'Turkish', 'تركي'),
(227, 'TM', 'Turkmenistan', 'تركمانستان', 'Turkmen', 'تركمانستاني'),
(228, 'TC', 'Turks and Caicos Islands', 'جزر توركس وكايكوس', 'Turks and Caicos Islands', 'جزر توركس وكايكوس'),
(229, 'TV', 'Tuvalu', 'توفالو', 'Tuvaluan', 'توفالي'),
(230, 'UG', 'Uganda', 'أوغندا', 'Ugandan', 'أوغندي'),
(231, 'UA', 'Ukraine', 'أوكرانيا', 'Ukrainian', 'أوكراني'),
(232, 'AE', 'United Arab Emirates', 'الإمارات العربية المتحدة', 'Emirati', 'إماراتي'),
(233, 'GB', 'United Kingdom', 'المملكة المتحدة', 'British', 'بريطاني'),
(234, 'US', 'United States', 'الولايات المتحدة', 'American', 'أمريكي'),
(235, 'UM', 'US Minor Outlying Islands', 'قائمة الولايات والمناطق الأمريكية', 'US Minor Outlying Islander', 'أمريكي'),
(236, 'UY', 'Uruguay', 'أورغواي', 'Uruguayan', 'أورغواي'),
(237, 'UZ', 'Uzbekistan', 'أوزباكستان', 'Uzbek', 'أوزباكستاني'),
(238, 'VU', 'Vanuatu', 'فانواتو', 'Vanuatuan', 'فانواتي'),
(239, 'VE', 'Venezuela', 'فنزويلا', 'Venezuelan', 'فنزويلي'),
(240, 'VN', 'Vietnam', 'فيتنام', 'Vietnamese', 'فيتنامي'),
(241, 'VI', 'Virgin Islands (U.S.)', 'الجزر العذراء الأمريكي', 'American Virgin Islander', 'أمريكي'),
(242, 'VA', 'Vatican City', 'فنزويلا', 'Vatican', 'فاتيكاني'),
(243, 'WF', 'Wallis and Futuna Islands', 'والس وفوتونا', 'Wallisian/Futunan', 'فوتوني'),
(244, 'EH', 'Western Sahara', 'الصحراء الغربية', 'Sahrawian', 'صحراوي'),
(245, 'YE', 'Yemen', 'اليمن', 'Yemeni', 'يمني'),
(246, 'ZM', 'Zambia', 'زامبيا', 'Zambian', 'زامبياني'),
(247, 'ZW', 'Zimbabwe', 'زمبابوي', 'Zimbabwean', 'زمبابوي');

-- --------------------------------------------------------

--
-- Table structure for table `country_info`
--

CREATE TABLE `country_info` (
  `id` int(11) NOT NULL,
  `iso` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nicename` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country_info`
--

INSERT INTO `country_info` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'الاسم', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(3, 1, 'email', 'text', 'البريد الاكتروني', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(4, 1, 'password', 'password', 'كلمة المرور', 1, 0, 0, 1, 1, 0, '{}', 4),
(6, 1, 'created_at', 'timestamp', 'توقيت الاضافة', 0, 1, 1, 0, 0, 0, '{}', 23),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(8, 1, 'avatar', 'image', 'الصورة الشخصية', 0, 1, 1, 1, 1, 1, '{}', 10),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 17),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(21, 1, 'role_id', 'hidden', 'Role', 0, 0, 0, 1, 1, 0, '{}', 12),
(22, 1, 'email_verified_at', 'timestamp', 'email_verified_at', 0, 0, 0, 0, 0, 0, '{}', 7),
(49, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 5, 'question', 'text', 'السؤال', 1, 1, 1, 1, 1, 1, '{}', 2),
(51, 5, 'answer', 'rich_text_box', 'الاجابة', 1, 0, 1, 1, 1, 1, '{}', 3),
(52, 5, 'order', 'hidden', 'الترتيب', 1, 1, 1, 1, 1, 1, '{}', 4),
(53, 5, 'created_at', 'timestamp', 'توقيت الاضافة', 1, 1, 1, 0, 0, 1, '{}', 5),
(54, 5, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 6),
(62, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(63, 8, 'title', 'text', 'العنوان', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(64, 8, 'text', 'rich_text_box', 'النص', 0, 0, 1, 1, 1, 1, '{}', 4),
(65, 8, 'image', 'image', 'الصورة', 1, 1, 1, 1, 1, 1, '{}', 3),
(66, 8, 'order', 'hidden', 'الترتيب', 1, 1, 1, 1, 1, 1, '{}', 5),
(67, 8, 'created_at', 'timestamp', 'توقيت الاضافة', 1, 1, 1, 0, 0, 1, '{}', 6),
(68, 8, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 7),
(69, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 9, 'slug', 'text', 'عنوان فريد', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:pages\"},\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(71, 9, 'title', 'text', 'العنوان', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(72, 9, 'body', 'rich_text_box', 'المحتوى', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(73, 9, 'status', 'checkbox', 'الحالة', 1, 1, 1, 1, 1, 1, '{\"on\":\"\\u0646\\u0634\\u0637\",\"off\":\"\\u063a\\u064a\\u0631 \\u0646\\u0634\\u0637\",\"checked\":\"true\"}', 5),
(74, 9, 'order', 'hidden', 'الترتيب', 1, 1, 1, 1, 1, 1, '{}', 6),
(75, 9, 'created_at', 'timestamp', 'توقيت الاضافة', 1, 1, 1, 0, 0, 1, '{}', 7),
(76, 9, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 8),
(143, 1, 'phone', 'text', 'رقم الهاتف', 0, 1, 1, 1, 1, 1, '{}', 6),
(144, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(145, 10, 'title', 'text', 'العنوان', 1, 1, 1, 1, 1, 1, '{}', 2),
(146, 10, 'description', 'rich_text_box', 'الوصف', 1, 0, 1, 1, 1, 1, '{}', 3),
(147, 10, 'created_at', 'timestamp', 'توقيت الاضافة', 0, 1, 1, 0, 0, 1, '{}', 4),
(148, 10, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 5),
(149, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(150, 11, 'name', 'text', 'الاسم', 1, 1, 1, 1, 1, 1, '{}', 2),
(151, 11, 'slug', 'text', 'اسم فريد', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 3),
(152, 11, 'order', 'hidden', 'الترتيب', 1, 1, 1, 0, 1, 1, '{}', 4),
(153, 11, 'view_count', 'text', 'عدد الزيارات', 1, 1, 1, 0, 0, 1, '{}', 5),
(154, 11, 'created_at', 'timestamp', 'توقيت الاضافة', 1, 1, 1, 0, 0, 1, '{}', 6),
(155, 11, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 7),
(156, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(157, 12, 'category_id', 'text', 'Category Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(158, 12, 'name', 'text', 'الاسم', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(159, 12, 'slug', 'text', 'اسم فريد', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 4),
(160, 12, 'image', 'image', 'صورة المنتج', 1, 1, 1, 1, 1, 1, '{}', 6),
(161, 12, 'description', 'rich_text_box', 'الوصف', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 7),
(162, 12, 'display_home', 'checkbox', 'يظهر في الرئيسية', 1, 1, 1, 1, 1, 1, '{\"on\":\"\\u0646\\u0634\\u0637\",\"off\":\"\\u063a\\u064a\\u0631 \\u0646\\u0634\\u0637\",\"checked\":\"true\"}', 8),
(163, 12, 'features', 'list', 'المميزات', 0, 0, 1, 1, 1, 1, '{}', 9),
(164, 12, 'created_at', 'timestamp', 'توقيت الاضافة', 1, 1, 1, 0, 0, 1, '{}', 10),
(165, 12, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '{}', 11),
(166, 12, 'product_belongsto_category_relationship', 'relationship', 'القسم', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'admins', 'مدير الموقع', 'مديري الموقع', 'voyager-person', 'App\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'App\\Http\\Controllers\\Dashboard\\AdminController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-10-16 09:29:29', '2019-11-18 09:36:47'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(3, 'roles', 'roles', 'الصلاحية', 'الصلاحيات', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-10-16 09:29:29', '2019-10-16 10:48:08'),
(5, 'common_questions', 'common-questions', 'سؤال شائع', 'الاسئلة الشائعة', 'voyager-pen', 'App\\CommonQuestion', NULL, 'App\\Http\\Controllers\\Dashboard\\CommonQuestionController', NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"question\",\"order_direction\":\"asc\",\"default_search_key\":\"question\",\"scope\":null}', '2019-10-22 15:43:25', '2019-11-18 09:39:27'),
(8, 'theme_sliders', 'theme-sliders', 'سليدر', 'سليدر', 'voyager-photos', 'App\\ThemeSlider', NULL, 'App\\Http\\Controllers\\Dashboard\\ThemeSliderController', NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"title\",\"order_direction\":\"asc\",\"default_search_key\":\"title\",\"scope\":null}', '2019-10-23 09:19:00', '2019-11-18 12:46:16'),
(9, 'pages', 'pages', 'صفحة', 'صفحات الموقع', 'voyager-logbook', 'App\\Page', NULL, 'App\\Http\\Controllers\\Dashboard\\PageController', NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"title\",\"order_direction\":\"asc\",\"default_search_key\":\"title\",\"scope\":null}', '2019-10-23 09:34:34', '2019-11-18 09:41:54'),
(10, 'jobs', 'jobs', 'وظيفة', 'الوظائف', 'voyager-certificate', 'App\\Job', NULL, 'App\\Http\\Controllers\\Dashboard\\JobController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-11-18 13:23:04', '2019-11-18 13:41:06'),
(11, 'categories', 'categories', 'قسم', 'اقسام', 'voyager-bar-chart', 'App\\Category', NULL, 'App\\Http\\Controllers\\Dashboard\\CategoryController', NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"name\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-11-18 13:29:33', '2019-11-18 13:37:02'),
(12, 'products', 'products', 'منتج', 'المنتجات', 'voyager-lab', 'App\\Product', NULL, 'App\\Http\\Controllers\\Dashboard\\ProductController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-11-18 14:03:00', '2019-11-18 17:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'محاسب', '<p><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span></strong><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></strong></p>', '2019-11-18 13:40:50', '2019-11-18 13:40:50');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-10-16 09:29:29', '2019-10-16 09:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'الرئيسية', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2019-10-16 09:29:29', '2019-10-16 10:32:35', 'voyager.dashboard', 'null'),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 8, '2019-10-16 09:29:29', '2019-11-18 14:06:46', 'voyager.media.index', NULL),
(4, 1, 'الصلاحيات', '', '_self', 'voyager-lock', '#000000', NULL, 7, '2019-10-16 09:29:29', '2019-11-18 14:06:50', 'voyager.roles.index', 'null'),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-10-16 09:29:29', '2019-11-18 14:06:46', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-10-16 09:29:29', '2019-10-20 09:23:50', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-10-16 09:29:29', '2019-10-20 09:23:50', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-10-16 09:29:29', '2019-10-20 09:23:50', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-10-16 09:29:29', '2019-10-20 09:23:50', 'voyager.bread.index', NULL),
(10, 1, 'الاعدادات', '', '_self', 'voyager-settings', '#000000', NULL, 10, '2019-10-16 09:29:29', '2019-11-18 14:06:46', 'voyager.settings.index', 'null'),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-10-16 09:29:30', '2019-10-20 09:23:50', 'voyager.hooks', NULL),
(18, 1, 'الاسئلة الشائعة', '', '_self', 'voyager-pen', '#000000', 19, 1, '2019-10-22 15:43:25', '2019-11-18 09:32:28', 'voyager.common-questions.index', 'null'),
(19, 1, 'واجهة الموقع', '#', '_self', 'voyager-documentation', '#000000', NULL, 6, '2019-10-23 08:10:26', '2019-11-18 14:06:50', NULL, ''),
(21, 1, 'سليدر', '', '_self', 'voyager-photos', '#000000', 19, 2, '2019-10-23 09:13:19', '2019-11-18 13:24:33', 'voyager.theme-sliders.index', 'null'),
(22, 1, 'صفحات الموقع', '', '_self', 'voyager-logbook', '#000000', 19, 3, '2019-10-23 09:34:34', '2019-11-18 13:24:33', 'voyager.pages.index', 'null'),
(23, 1, 'اعدادات الموقع', '', '_self', 'voyager-settings', '#000000', 19, 4, '2019-10-23 10:24:29', '2019-11-18 13:24:33', 'voyager.theme-settings.index', 'null'),
(33, 1, 'مديري الموقع', 'admin/admins/', '_self', 'voyager-person', '#000000', NULL, 2, '2019-11-17 17:06:48', '2019-11-18 13:31:43', NULL, ''),
(34, 1, 'الوظائف', '', '_self', 'voyager-certificate', '#000000', NULL, 5, '2019-11-18 13:23:04', '2019-11-18 14:06:50', 'voyager.jobs.index', 'null'),
(35, 1, 'اقسام', '', '_self', 'voyager-bar-chart', '#000000', NULL, 3, '2019-11-18 13:29:33', '2019-11-18 13:42:06', 'voyager.categories.index', 'null'),
(36, 1, 'المنتجات', '', '_self', 'voyager-lab', '#000000', NULL, 4, '2019-11-18 14:03:01', '2019-11-18 14:06:50', 'voyager.products.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `title`, `body`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'من-نحن', 'من نحن', '<p style=\"text-align: right;\"><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span></strong><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></strong><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</span></strong><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</span></strong></p>\n<p style=\"text-align: right;\"><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><img src=\"http://localhost/ArtisticPlatform/public/storage/pages/October2019/1430368724442417087yellow%20orange%20peach%20pink%20blur%20wallpaper%20android%20background%20mixed%20combiantion%20plus%20radiant%20gradient-hi.png\" alt=\"\" width=\"1210\" height=\"1620\" /></span></strong></p>', 1, 1, '2019-10-23 09:54:38', '2019-11-18 12:53:25'),
(3, 'سياسة-الخصوصية', 'سياسة الخصوصية', '<p><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</span></p>', 1, 3, '2019-10-23 10:12:29', '2019-10-23 10:12:29'),
(4, 'الشروط-والاحكام', 'الشروط والاحكام', '<p><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</span></p>', 1, 4, '2019-10-23 10:12:55', '2019-11-18 12:59:18'),
(7, 'المستويات', 'المستويات', '<p><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></p>', 0, 7, '2019-10-23 10:14:49', '2019-10-23 10:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(2, 'browse_bread', NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(3, 'browse_database', NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(4, 'browse_media', NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(5, 'browse_compass', NULL, '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(6, 'browse_menus', 'menus', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(7, 'read_menus', 'menus', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(8, 'edit_menus', 'menus', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(9, 'add_menus', 'menus', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(10, 'delete_menus', 'menus', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(11, 'browse_roles', 'roles', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(12, 'read_roles', 'roles', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(13, 'edit_roles', 'roles', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(14, 'add_roles', 'roles', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(15, 'delete_roles', 'roles', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(16, 'browse_admins', 'users', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(17, 'read_admins', 'users', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(18, 'edit_admins', 'users', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(19, 'add_admins', 'users', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(20, 'delete_admins', 'users', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(21, 'browse_settings', 'settings', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(22, 'read_settings', 'settings', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(23, 'edit_settings', 'settings', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(24, 'add_settings', 'settings', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(25, 'delete_settings', 'settings', '2019-10-16 09:29:29', '2019-10-16 09:29:29'),
(26, 'browse_hooks', NULL, '2019-10-16 09:29:30', '2019-10-16 09:29:30'),
(27, 'browse_users', 'users', '2019-10-16 13:26:04', '2019-10-16 13:26:04'),
(28, 'read_users', 'users', '2019-10-16 13:26:04', '2019-10-16 13:26:04'),
(29, 'edit_users', 'users', '2019-10-16 13:26:04', '2019-10-16 13:26:04'),
(30, 'add_users', 'users', '2019-10-16 13:26:04', '2019-10-16 13:26:04'),
(31, 'delete_users', 'users', '2019-10-16 13:26:04', '2019-10-16 13:26:04'),
(37, 'browse_common_questions', 'common_questions', '2019-10-22 15:43:25', '2019-10-22 15:43:25'),
(38, 'read_common_questions', 'common_questions', '2019-10-22 15:43:25', '2019-10-22 15:43:25'),
(39, 'edit_common_questions', 'common_questions', '2019-10-22 15:43:25', '2019-10-22 15:43:25'),
(40, 'add_common_questions', 'common_questions', '2019-10-22 15:43:25', '2019-10-22 15:43:25'),
(41, 'delete_common_questions', 'common_questions', '2019-10-22 15:43:25', '2019-10-22 15:43:25'),
(47, 'browse_theme_sliders', 'theme-sliders', '2019-10-23 09:13:19', '2019-10-23 09:13:19'),
(48, 'read_theme_sliders', 'theme-sliders', '2019-10-23 09:13:19', '2019-10-23 09:13:19'),
(49, 'edit_theme_sliders', 'theme-sliders', '2019-10-23 09:13:19', '2019-10-23 09:13:19'),
(50, 'add_theme_sliders', 'theme-sliders', '2019-10-23 09:13:19', '2019-10-23 09:13:19'),
(51, 'delete_theme_sliders', 'theme-sliders', '2019-10-23 09:13:19', '2019-10-23 09:13:19'),
(52, 'browse_theme_sliders', 'theme_sliders', '2019-10-23 09:19:00', '2019-10-23 09:19:00'),
(53, 'read_theme_sliders', 'theme_sliders', '2019-10-23 09:19:00', '2019-10-23 09:19:00'),
(54, 'edit_theme_sliders', 'theme_sliders', '2019-10-23 09:19:00', '2019-10-23 09:19:00'),
(55, 'add_theme_sliders', 'theme_sliders', '2019-10-23 09:19:00', '2019-10-23 09:19:00'),
(56, 'delete_theme_sliders', 'theme_sliders', '2019-10-23 09:19:00', '2019-10-23 09:19:00'),
(57, 'browse_pages', 'pages', '2019-10-23 09:34:34', '2019-10-23 09:34:34'),
(58, 'read_pages', 'pages', '2019-10-23 09:34:34', '2019-10-23 09:34:34'),
(59, 'edit_pages', 'pages', '2019-10-23 09:34:34', '2019-10-23 09:34:34'),
(60, 'add_pages', 'pages', '2019-10-23 09:34:34', '2019-10-23 09:34:34'),
(61, 'delete_pages', 'pages', '2019-10-23 09:34:34', '2019-10-23 09:34:34'),
(62, 'browse_jobs', 'jobs', '2019-11-18 13:23:04', '2019-11-18 13:23:04'),
(63, 'read_jobs', 'jobs', '2019-11-18 13:23:04', '2019-11-18 13:23:04'),
(64, 'edit_jobs', 'jobs', '2019-11-18 13:23:04', '2019-11-18 13:23:04'),
(65, 'add_jobs', 'jobs', '2019-11-18 13:23:04', '2019-11-18 13:23:04'),
(66, 'delete_jobs', 'jobs', '2019-11-18 13:23:04', '2019-11-18 13:23:04'),
(67, 'browse_categories', 'categories', '2019-11-18 13:29:33', '2019-11-18 13:29:33'),
(68, 'read_categories', 'categories', '2019-11-18 13:29:33', '2019-11-18 13:29:33'),
(69, 'edit_categories', 'categories', '2019-11-18 13:29:33', '2019-11-18 13:29:33'),
(70, 'add_categories', 'categories', '2019-11-18 13:29:33', '2019-11-18 13:29:33'),
(71, 'delete_categories', 'categories', '2019-11-18 13:29:33', '2019-11-18 13:29:33'),
(72, 'browse_products', 'products', '2019-11-18 14:03:00', '2019-11-18 14:03:00'),
(73, 'read_products', 'products', '2019-11-18 14:03:00', '2019-11-18 14:03:00'),
(74, 'edit_products', 'products', '2019-11-18 14:03:00', '2019-11-18 14:03:00'),
(75, 'add_products', 'products', '2019-11-18 14:03:00', '2019-11-18 14:03:00'),
(76, 'delete_products', 'products', '2019-11-18 14:03:00', '2019-11-18 14:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5),
(10, 5),
(11, 5),
(12, 5),
(13, 5),
(14, 5),
(15, 5),
(16, 1),
(16, 5),
(17, 1),
(17, 5),
(18, 1),
(18, 5),
(19, 1),
(19, 5),
(20, 1),
(20, 5),
(21, 1),
(21, 5),
(22, 1),
(22, 5),
(23, 1),
(23, 5),
(24, 1),
(24, 5),
(25, 1),
(25, 5),
(26, 5),
(27, 1),
(27, 5),
(28, 1),
(28, 5),
(29, 1),
(29, 5),
(30, 1),
(30, 5),
(31, 1),
(31, 5),
(37, 1),
(37, 5),
(38, 1),
(38, 5),
(39, 1),
(39, 5),
(40, 1),
(40, 5),
(41, 1),
(41, 5),
(47, 1),
(47, 5),
(48, 1),
(48, 5),
(49, 1),
(49, 5),
(50, 1),
(50, 5),
(51, 1),
(51, 5),
(52, 1),
(52, 5),
(53, 1),
(53, 5),
(54, 1),
(54, 5),
(55, 1),
(55, 5),
(56, 1),
(56, 5),
(57, 1),
(57, 5),
(58, 1),
(58, 5),
(59, 1),
(59, 5),
(60, 1),
(60, 5),
(61, 1),
(61, 5),
(62, 1),
(62, 5),
(63, 1),
(63, 5),
(64, 1),
(64, 5),
(65, 1),
(65, 5),
(66, 1),
(66, 5),
(67, 1),
(67, 5),
(68, 1),
(68, 5),
(69, 1),
(69, 5),
(70, 1),
(70, 5),
(71, 1),
(71, 5),
(72, 1),
(72, 5),
(73, 1),
(73, 5),
(74, 1),
(74, 5),
(75, 1),
(75, 5),
(76, 1),
(76, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `display_home` int(1) NOT NULL DEFAULT '1',
  `features` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `slug`, `image`, `description`, `display_home`, `features`, `created_at`, `updated_at`) VALUES
(1, 3, 'منتج 1', 'منتج-1', 'products\\November2019\\h6ocTPZcKUAtduLfw4dE.png', '<p><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span></strong><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></strong></p>', 1, '{\"ar\":[\"\\u0645\\u064a\\u0632\\u0629\",\"\\u0645\\u064a\\u0632\\u0629\"],\"en\":[\"Feature\",\"Feature\",\"Feature\"]}', '2019-11-18 19:01:50', '2019-11-18 19:41:52'),
(2, 7, 'منتج 2', 'منتج-2', 'products\\November2019\\qLUmehN71MURPnlJSPze.jpg', '<p><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></p>', 1, '{\"ar\":[null],\"en\":[null]}', '2019-11-18 19:17:18', '2019-11-18 19:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'مدير الموقع', '2019-10-16 09:29:29', '2019-10-16 10:53:40'),
(2, 'user', 'مستخدم', '2019-10-16 09:29:29', '2019-10-16 10:53:47'),
(5, 'developer', 'خاص بالمبرمج', '2019-10-23 10:35:39', '2019-10-23 10:35:39');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(5, 'admin-bg_image', 'صورة لوحة التحكم', 'settings/rp76EWXIrSuKcnGGfbU2XwxiVViTjlMhJVjgQhun.jpeg', '', 'image', 5, 'Admin'),
(6, 'admin-title-ar', 'عنوان لوحة التحكم', 'لوحة تحكم الفريد البرونزي', '', 'text', 1, NULL),
(7, 'admin-description-ar', 'وصف لوحة التحكم', 'شركه تعمل في تجاره الحاصلات الزراعية وتوريدها للمصانع لاستخدامها في تصنيع الاعلاف الخاصة بالاسماك والمواشي.', '', 'text', 2, NULL),
(9, 'admin-icon_image', 'ايقون لوحة التحكم', 'settings/ZJdnmQKTR8kdpKlR6QhvjTm30iqQ7G5WxAxZVaTb.png', '', 'image', 4, 'Admin'),
(11, 'admin-color', '', '#008080', NULL, NULL, 1, NULL),
(12, 'theme-color', NULL, '#408080', NULL, NULL, 1, NULL),
(13, 'theme-icon_image-ar', NULL, 'settings/K9aRe7dMCsoZzq1O5TtfIHev9itN7rpNAgGQm4tE.png', NULL, NULL, 1, NULL),
(14, 'theme-font-color', NULL, '#ff0080', NULL, NULL, 1, NULL),
(15, 'theme-paginate-count', NULL, '15', NULL, NULL, 1, NULL),
(16, 'theme-name-ar', NULL, 'الفريد البرونزي', NULL, NULL, 1, NULL),
(17, 'theme-keywords-ar', NULL, 'تجاره,الحاصلات الزراعية,الاعلاف,الاسماك,المواشي', NULL, NULL, 1, NULL),
(18, 'theme-description-ar', NULL, 'شركه تعمل في تجاره الحاصلات الزراعية وتوريدها للمصانع لاستخدامها في تصنيع الاعلاف الخاصة بالاسماك والمواشي.', NULL, NULL, 1, NULL),
(19, 'email', NULL, 'admin@admin.com', NULL, NULL, 1, NULL),
(20, 'phone', NULL, '0123456789', NULL, NULL, 1, NULL),
(21, 'address', NULL, 'الرياض, المملكة العريبة السعودية', NULL, NULL, 1, NULL),
(22, 'theme-default-avatar', NULL, 'settings/7oLiXMOa2ebPxmg0jB30UlV7kPbjY28sIn7041wi.png', NULL, NULL, 1, NULL),
(23, 'facebook', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(24, 'twitter', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(25, 'telegram', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(26, 'youtube', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(27, 'snapchat', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(28, 'instagram', NULL, 'https://www.facebook.com/', NULL, NULL, 1, NULL),
(29, 'theme-name-en', NULL, 'al Farid', NULL, NULL, 1, NULL),
(30, 'theme-keywords-en', NULL, 'commerce,business', NULL, NULL, 1, NULL),
(31, 'theme-description-en', NULL, 'business business business business business business business business business business business business business business business', NULL, NULL, 1, NULL),
(32, 'theme-icon_image-en', NULL, 'settings/9AINHwX0vpmY8gVfwrFZWsrtOXTZHO3LYM6Wqqa3.png', NULL, NULL, 1, NULL),
(33, 'admin-title-en', NULL, 'Al Farid', NULL, NULL, 1, NULL),
(34, 'admin-description-en', NULL, 'description description description description description description description description description', NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `theme_sliders`
--

CREATE TABLE `theme_sliders` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `theme_sliders`
--

INSERT INTO `theme_sliders` (`id`, `title`, `text`, `image`, `order`, `created_at`, `updated_at`) VALUES
(1, 'سليدر 1', '<h2><strong>هنا يكتب نص يظهر فوق الصورة</strong></h2>\n<div id=\"gtx-trans\" style=\"position: absolute; left: 281px; top: -10.5868px;\">\n<div class=\"gtx-trans-icon\">&nbsp;</div>\n</div>', 'theme-sliders\\October2019\\AewBI95HYLZ59tTZx9pG.jpg', 1, '2019-10-23 09:19:40', '2019-11-18 12:45:33'),
(2, 'سليدر 2', '<p>سليددددددددددددر 2</p>', 'theme-sliders\\October2019\\uaGHBVjTr90Z7jE8Sdku.png', 2, '2019-10-23 09:20:07', '2019-11-18 12:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_rows', 'display_name', 1, 'en', 'ID', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(2, 'data_rows', 'display_name', 21, 'en', 'Role', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(3, 'data_rows', 'display_name', 2, 'ar', 'الاسم', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(4, 'data_rows', 'display_name', 3, 'ar', 'البريد الاكتروني', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(5, 'data_rows', 'display_name', 143, 'ar', 'رقم الهاتف', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(6, 'data_rows', 'display_name', 8, 'ar', 'الصورة الشخصية', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(7, 'data_rows', 'display_name', 22, 'en', 'email_verified_at', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(8, 'data_rows', 'display_name', 4, 'ar', 'كلمة المرور', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(9, 'data_rows', 'display_name', 11, 'en', 'Settings', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(10, 'data_rows', 'display_name', 6, 'ar', 'توقيت الاضافة', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(11, 'data_rows', 'display_name', 7, 'en', 'Updated At', '2019-11-18 08:54:03', '2019-11-18 08:54:03'),
(12, 'data_types', 'display_name_singular', 1, 'ar', 'مدير الموقع', '2019-11-18 08:54:04', '2019-11-18 08:54:04'),
(13, 'data_types', 'display_name_plural', 1, 'ar', 'مديري الموقع', '2019-11-18 08:54:04', '2019-11-18 08:54:04'),
(14, 'menu_items', 'title', 1, 'en', 'Dashboard', NULL, NULL),
(15, 'menu_items', 'title', 2, 'ar', 'ميديا', NULL, NULL),
(16, 'menu_items', 'title', 33, 'en', 'Admins', NULL, NULL),
(17, 'menu_items', 'title', 19, 'en', 'Website', '2019-11-18 09:30:46', '2019-11-18 09:30:46'),
(18, 'menu_items', 'title', 18, 'en', 'Common Questions', '2019-11-18 09:32:28', '2019-11-18 09:32:28'),
(19, 'menu_items', 'title', 21, 'en', 'Slider', '2019-11-18 09:33:58', '2019-11-18 09:33:58'),
(20, 'menu_items', 'title', 22, 'en', 'Pages', '2019-11-18 09:34:12', '2019-11-18 09:34:12'),
(21, 'menu_items', 'title', 23, 'en', 'Website Settings', '2019-11-18 09:34:32', '2019-11-18 09:34:32'),
(22, 'menu_items', 'title', 4, 'en', 'Roles', '2019-11-18 09:34:40', '2019-11-18 09:34:40'),
(23, 'menu_items', 'title', 10, 'en', 'Settings', '2019-11-18 09:34:55', '2019-11-18 09:34:55'),
(24, 'data_rows', 'display_name', 2, 'en', 'Name', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(25, 'data_rows', 'display_name', 3, 'en', 'Email', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(26, 'data_rows', 'display_name', 143, 'en', 'Phone', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(27, 'data_rows', 'display_name', 8, 'en', 'Avatar', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(28, 'data_rows', 'display_name', 4, 'en', 'Password', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(29, 'data_rows', 'display_name', 6, 'en', 'Created at', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(30, 'data_types', 'display_name_singular', 1, 'en', 'Admin', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(31, 'data_types', 'display_name_plural', 1, 'en', 'Admins', '2019-11-18 09:36:47', '2019-11-18 09:36:47'),
(32, 'data_rows', 'display_name', 49, 'en', 'Id', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(33, 'data_rows', 'display_name', 50, 'en', 'Question', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(34, 'data_rows', 'display_name', 51, 'en', 'Answer', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(35, 'data_rows', 'display_name', 52, 'en', 'Order', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(36, 'data_rows', 'display_name', 53, 'en', 'Created at', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(37, 'data_rows', 'display_name', 54, 'en', 'Updated At', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(38, 'data_types', 'display_name_singular', 5, 'en', 'Common Question', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(39, 'data_types', 'display_name_plural', 5, 'en', 'Common Questions', '2019-11-18 09:39:27', '2019-11-18 09:39:27'),
(40, 'data_rows', 'display_name', 62, 'en', 'Id', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(41, 'data_rows', 'display_name', 63, 'en', 'Title', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(42, 'data_rows', 'display_name', 64, 'en', 'Text', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(43, 'data_rows', 'display_name', 65, 'en', 'Image', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(44, 'data_rows', 'display_name', 66, 'en', 'Order', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(45, 'data_rows', 'display_name', 67, 'en', 'Created at', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(46, 'data_rows', 'display_name', 68, 'en', 'Updated At', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(47, 'data_types', 'display_name_singular', 8, 'en', 'Slider', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(48, 'data_types', 'display_name_plural', 8, 'en', 'Sliders', '2019-11-18 09:40:21', '2019-11-18 09:40:21'),
(49, 'data_rows', 'display_name', 69, 'en', 'Id', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(50, 'data_rows', 'display_name', 70, 'en', 'slug', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(51, 'data_rows', 'display_name', 71, 'en', 'Title', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(52, 'data_rows', 'display_name', 72, 'en', 'Content', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(53, 'data_rows', 'display_name', 73, 'en', 'Status', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(54, 'data_rows', 'display_name', 74, 'en', 'Order', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(55, 'data_rows', 'display_name', 75, 'en', 'Created at', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(56, 'data_rows', 'display_name', 76, 'en', 'Updated At', '2019-11-18 09:41:54', '2019-11-18 09:41:54'),
(57, 'data_types', 'display_name_singular', 9, 'en', 'Page', '2019-11-18 09:41:55', '2019-11-18 09:41:55'),
(58, 'data_types', 'display_name_plural', 9, 'en', 'Pages', '2019-11-18 09:41:55', '2019-11-18 09:41:55'),
(59, 'common_questions', 'question', 1, 'en', 'Just Ask', '2019-11-18 12:36:54', '2019-11-18 12:36:54'),
(60, 'common_questions', 'answer', 1, 'en', '<p><strong>Askkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk</strong></p>', '2019-11-18 12:36:54', '2019-11-18 12:36:54'),
(61, 'common_questions', 'question', 3, 'en', 'New Ask', '2019-11-18 12:38:12', '2019-11-18 12:38:12'),
(62, 'common_questions', 'answer', 3, 'en', '<p><strong>New Ask</strong></p>', '2019-11-18 12:38:12', '2019-11-18 12:38:12'),
(63, 'common_questions', 'question', 4, 'en', 'What is the goal of the site ?', '2019-11-18 12:39:00', '2019-11-18 12:39:00'),
(64, 'common_questions', 'answer', 4, 'en', '<p><strong>What is the goal of the site&nbsp;What is the goal of the site&nbsp;What is the goal of the site&nbsp;What is the goal of the site&nbsp;What is the goal of the site</strong></p>\n<div id=\"gtx-trans\" style=\"position: absolute; left: 520px; top: 32.7708px;\">\n<div class=\"gtx-trans-icon\">&nbsp;</div>\n</div>', '2019-11-18 12:39:00', '2019-11-18 12:39:00'),
(65, 'common_questions', 'question', 2, 'en', 'ASSSSSK', '2019-11-18 12:40:06', '2019-11-18 12:40:06'),
(66, 'common_questions', 'answer', 2, 'en', '<p style=\"text-align: center;\">ASKKKKKKKKKKK</p>\n<div id=\"gtx-trans\" style=\"position: absolute; left: 748px; top: 32.7708px;\">\n<div class=\"gtx-trans-icon\">&nbsp;</div>\n</div>', '2019-11-18 12:40:06', '2019-11-18 12:40:06'),
(67, 'theme_sliders', 'title', 1, 'en', 'slider 1', '2019-11-18 12:44:52', '2019-11-18 12:47:29'),
(68, 'theme_sliders', 'text', 1, 'en', '<h2><strong>Write Text</strong></h2>\n<div id=\"gtx-trans\" style=\"position: absolute; left: 286px; top: 43.9688px;\">\n<div class=\"gtx-trans-icon\">&nbsp;</div>\n</div>', '2019-11-18 12:44:52', '2019-11-18 12:45:33'),
(69, 'theme_sliders', 'title', 2, 'en', 'Slider 2', '2019-11-18 12:47:13', '2019-11-18 12:47:13'),
(70, 'theme_sliders', 'text', 2, 'en', '<p>Slider 2</p>', '2019-11-18 12:47:13', '2019-11-18 12:47:13'),
(71, 'pages', 'slug', 1, 'en', 'about-us', '2019-11-18 12:53:25', '2019-11-18 12:53:25'),
(72, 'pages', 'title', 1, 'en', 'About us', '2019-11-18 12:53:25', '2019-11-18 12:53:25'),
(73, 'pages', 'body', 1, 'en', '<p style=\"text-align: right;\"><strong style=\"margin: 0px; padding: 0px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">.</span></strong></p>\n<p style=\"text-align: right;\"><strong><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\"><img src=\"http://localhost/ArtisticPlatform/public/storage/pages/October2019/1430368724442417087yellow%20orange%20peach%20pink%20blur%20wallpaper%20android%20background%20mixed%20combiantion%20plus%20radiant%20gradient-hi.png\" alt=\"\" width=\"1210\" height=\"1620\" /></span></strong></p>', '2019-11-18 12:53:25', '2019-11-18 12:53:25'),
(74, 'pages', 'slug', 3, 'en', 'privacy-policy', '2019-11-18 12:55:01', '2019-11-18 12:55:01'),
(75, 'pages', 'title', 3, 'en', 'Privacy policy', '2019-11-18 12:55:01', '2019-11-18 12:55:01'),
(76, 'pages', 'body', 3, 'en', '<p>Lorem Ipsum<span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '2019-11-18 12:55:01', '2019-11-18 12:55:01'),
(77, 'pages', 'slug', 4, 'en', 'terms-and-conditions', '2019-11-18 12:59:18', '2019-11-18 12:59:18'),
(78, 'pages', 'title', 4, 'en', 'Terms and Conditions', '2019-11-18 12:59:18', '2019-11-18 12:59:18'),
(79, 'pages', 'body', 4, 'en', '<p>Lorem Ipsum<span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p>', '2019-11-18 12:59:18', '2019-11-18 12:59:18'),
(80, 'pages', 'slug', 7, 'en', 'lavels', '2019-11-18 13:04:58', '2019-11-18 13:04:58'),
(81, 'pages', 'title', 7, 'en', 'Lavels', '2019-11-18 13:04:58', '2019-11-18 13:04:58'),
(82, 'pages', 'body', 7, 'en', '<p><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</span><br class=\"line-break\" style=\"margin-bottom: 27px; color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\" /><span style=\"color: #000000; font-family: JannaLT-Regular; font-size: 18px; text-align: right;\">إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</span></p>', '2019-11-18 13:04:58', '2019-11-18 13:04:58'),
(83, 'data_rows', 'display_name', 144, 'en', 'Id', '2019-11-18 13:23:56', '2019-11-18 13:23:56'),
(84, 'data_rows', 'display_name', 145, 'en', 'Title', '2019-11-18 13:23:56', '2019-11-18 13:23:56'),
(85, 'data_rows', 'display_name', 146, 'en', 'Description', '2019-11-18 13:23:56', '2019-11-18 13:23:56'),
(86, 'data_rows', 'display_name', 147, 'en', 'Created at', '2019-11-18 13:23:56', '2019-11-18 13:23:56'),
(87, 'data_rows', 'display_name', 148, 'en', 'Updated At', '2019-11-18 13:23:56', '2019-11-18 13:23:56'),
(88, 'data_types', 'display_name_singular', 10, 'en', 'Job', '2019-11-18 13:23:56', '2019-11-18 13:30:57'),
(89, 'data_types', 'display_name_plural', 10, 'en', 'Jobs', '2019-11-18 13:23:56', '2019-11-18 13:30:57'),
(90, 'data_rows', 'display_name', 149, 'en', 'Id', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(91, 'data_rows', 'display_name', 150, 'en', 'Name', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(92, 'data_rows', 'display_name', 151, 'en', 'Slug', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(93, 'data_rows', 'display_name', 152, 'en', 'Order', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(94, 'data_rows', 'display_name', 153, 'en', 'View Count', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(95, 'data_rows', 'display_name', 154, 'en', 'Created at', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(96, 'data_rows', 'display_name', 155, 'en', 'Updated At', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(97, 'data_types', 'display_name_singular', 11, 'en', 'Category', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(98, 'data_types', 'display_name_plural', 11, 'en', 'Categories', '2019-11-18 13:30:35', '2019-11-18 13:30:35'),
(99, 'categories', 'name', 3, 'en', 'Fish', '2019-11-18 13:38:00', '2019-11-18 13:38:00'),
(100, 'categories', 'slug', 3, 'en', 'fish', '2019-11-18 13:38:00', '2019-11-18 13:38:00'),
(101, 'categories', 'name', 7, 'en', 'Livestock', '2019-11-18 13:39:19', '2019-11-18 13:39:19'),
(102, 'categories', 'slug', 7, 'en', 'livestock', '2019-11-18 13:39:19', '2019-11-18 13:39:19'),
(103, 'jobs', 'title', 1, 'en', 'Accountant', '2019-11-18 13:40:50', '2019-11-18 13:40:50'),
(104, 'jobs', 'description', 1, 'en', '<p><strong style=\"margin: 0px; padding: 0px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '2019-11-18 13:40:50', '2019-11-18 13:40:50'),
(105, 'menu_items', 'title', 35, 'en', 'Categories', '2019-11-18 13:42:06', '2019-11-18 13:42:06'),
(106, 'menu_items', 'title', 34, 'en', 'Jobs', '2019-11-18 13:42:15', '2019-11-18 13:42:15'),
(107, 'data_rows', 'display_name', 156, 'en', 'Id', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(108, 'data_rows', 'display_name', 157, 'en', 'Category Id', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(109, 'data_rows', 'display_name', 158, 'en', 'Name', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(110, 'data_rows', 'display_name', 159, 'en', 'Slug', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(111, 'data_rows', 'display_name', 160, 'en', 'Image', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(112, 'data_rows', 'display_name', 161, 'en', 'Description', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(113, 'data_rows', 'display_name', 162, 'en', 'Display Home', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(114, 'data_rows', 'display_name', 163, 'en', 'Features', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(115, 'data_rows', 'display_name', 164, 'en', 'Created at', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(116, 'data_rows', 'display_name', 165, 'en', 'Updated At', '2019-11-18 14:04:18', '2019-11-18 14:04:18'),
(117, 'data_types', 'display_name_singular', 12, 'en', 'Product', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(118, 'data_types', 'display_name_plural', 12, 'en', 'Products', '2019-11-18 14:04:18', '2019-11-18 14:09:20'),
(119, 'data_rows', 'display_name', 166, 'en', 'categories', '2019-11-18 14:05:42', '2019-11-18 14:05:42'),
(120, 'menu_items', 'title', 36, 'en', 'Prodcuts', '2019-11-18 14:05:57', '2019-11-18 14:05:57'),
(121, 'products', 'name', 1, 'en', 'Product 1', '2019-11-18 19:01:50', '2019-11-18 19:01:50'),
(122, 'products', 'slug', 1, 'en', 'product-1', '2019-11-18 19:01:50', '2019-11-18 19:01:50'),
(123, 'products', 'description', 1, 'en', '<p><strong>Lorem Ipsum<span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></strong></p>', '2019-11-18 19:01:50', '2019-11-18 19:01:50'),
(124, 'products', 'name', 2, 'en', 'Product 2', '2019-11-18 19:17:18', '2019-11-18 19:17:18'),
(125, 'products', 'slug', 2, 'en', 'product-2', '2019-11-18 19:17:18', '2019-11-18 19:17:18'),
(126, 'products', 'description', 2, 'en', '<p><strong style=\"margin: 0px; padding: 0px; color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p>', '2019-11-18 19:17:18', '2019-11-18 19:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `phone`, `avatar`, `email_verified_at`, `password`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', NULL, 'users/default.png', NULL, '$2y$10$O.MfGrN3o2s1d1rSsFBA7OTgaKOn7fLRlicYPlI2r3ygki/cCwHhW', '{\"locale\":\"en\"}', '2019-10-16 09:30:34', '2019-11-18 09:12:02'),
(12, 1, 'user', 'user@gmail.com', NULL, 'admins\\October2019\\vkalzvVGPtMqX7MnLRZV.png', NULL, '$2y$10$myszuwEUAVtrZ9kBcDdbBOMORGcZXq6k1Tn1cl/NgjUMfXu7ZPEiO', NULL, '2019-10-17 09:40:07', '2019-10-17 15:28:15'),
(24, 5, 'developer', 'developer@gmail.com', NULL, 'users/default.png', NULL, '$2y$10$xQpzuIyV55bP49xVIWWMj.vc/ejwYf0GHkU9eeXuO41S1xrVxvE2.', '{\"locale\":\"ar\"}', '2019-10-23 10:36:34', '2019-11-18 09:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `common_questions`
--
ALTER TABLE `common_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_info`
--
ALTER TABLE `country_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `theme_sliders`
--
ALTER TABLE `theme_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3667;

--
-- AUTO_INCREMENT for table `common_questions`
--
ALTER TABLE `common_questions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `country_info`
--
ALTER TABLE `country_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `theme_sliders`
--
ALTER TABLE `theme_sliders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

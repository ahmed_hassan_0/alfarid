<?php
/**
 * Created by PhpStorm.
 * User: Ahmed
 */

return [
    'arabic'        => 'عربي',
    'english'        => 'انجليزي',
    'theme-settings' => 'اعدادات تصميم الموقع',
    'icon-theme'     => 'ايقونة الموقع',
    'icon-theme-ar'     => 'ايقونة عربي',
    'icon-theme-en'     => 'ايقونة انجليزي',
    'color'          => 'اللون الرئيسي',
    'font-color'     => 'لون الخط',
    'paginate_count' => 'عدد العناصر في الصفحة',
    'buttons'        => [
        'save'   => 'حفظ'
    ],
    'website-data'   => 'معلومات الموقع',
    'website-name'   => 'اسم الموقع',
    'website-keywords' => 'كلمات مفتاحية',
    'website-desc'    => 'وصف الموقع',
    'contact-us'    => 'معلومات الاتصال',
    'email'    => 'البريد الالكتروني',
    'phone'    => 'رقم الهاتف',
    'address'    => 'العنوان',
    'facebook'    => 'فيس بوك',
    'twitter'    => 'تويتر',
    'telegram'    => 'تيلجرام',
    'youtube'    => 'يوتيوب',
    'snapchat'    => 'سناب شات',
    'instagram'    => 'انستجرام',
    'admin-theme-settings' => 'اعدادات التصميم',
    'image-dashboard' => 'صورة لوحة التحكم',
    'icon-dashboard' => 'ايقونة لوحة التحكم',
    'dashboard-fond-color' => 'اللون الاساسي للوحة التحكم',
    'dashboard-info' => 'معلومات لوحة التحكم',
    'dashboard-title' => 'عنوان لوحة التحكم',
    'dashboard-desc' => 'وصف لوحة التحكم',
    'add_new_field'  => 'أضف ميزة'
];
<?php
/**
 * Created by PhpStorm.
 * User: Ahmed
 */

return [
    'arabic'        => 'Arabic',
    'english'        => 'English',
    'theme-settings' => 'Theme Settings',
    'icon-theme'     => 'Website Icon',
    'icon-theme-ar'     => 'Website Icon Arabic',
    'icon-theme-en'     => 'Website Icon English',
    'color'          => 'Website Color',
    'font-color'     => 'Color Font',
    'paginate_count' => 'Paginate Count',
    'buttons'        => [
        'save'   => 'Save'
    ],
    'website-data'   => 'Website Info',
    'website-name'   => 'Website Name',
    'website-keywords' => 'Keywords',
    'website-desc'    => 'Description',
    'contact-us'    => 'Contact Us',
    'email'    => 'Email',
    'phone'    => 'Phone',
    'address'    => 'Address',
    'facebook'    => 'Facebook',
    'twitter'    => 'Twitter',
    'telegram'    => 'Telegram',
    'youtube'    => 'Youtube',
    'snapchat'    => 'Snapchat',
    'instagram'    => 'Instagram',
    'admin-theme-settings' => 'Admin Theme Settings',
    'icon-dashboard' => 'Dashboard Icon',
    'image-dashboard' => 'Dashboard Image',
    'dashboard-fond-color' => 'Dashboard Font Color',
    'dashboard-info' => 'Dashboard Info',
    'dashboard-title' => 'Dashboard Title',
    'dashboard-desc' => 'Dashboard Description',
    'add_new_field'  => 'New Feature'
];
@extends('theme.partials.master')

@section('pageTitle', 'الاستوديو')

@section('content')

    <h1>الاستوديو</h1>

    <h3>الفنانين</h3>
    <ul>
        @foreach($artists as $artist)
            <li><a href="{{ route('artists.show', ['slug' => $artist->username]) }}">{{ $artist->info->display_name }}</a></li>
        @endforeach
    </ul>
    <hr>

    <h3>المعارض</h3>
    <ul>
        @foreach($galleries as $gallery)
            <li><a href="{{ route('galleries.show', ['slug' => $gallery->username]) }}">{{ $gallery->info->display_name }}</a></li>
        @endforeach
    </ul>
    <hr>

    <h3>الفعاليات</h3>
    <ul>
        @foreach($events as $event)
            <li>
                <a href="{{ route('events.show', ['slug' => $event->slug]) }}">
                    <h3>{{ $event->title }}</h3>
                </a>
                <p>{{ $event->display_start_at->format('Y-m-d g:i A') }} <br> {{ $event->display_end_at->format('Y-m-d g:i A') }}</p>
                <small class="text-{{ $event->status['class'] }}">{{ $event->status['label'] }}</small>
            </li>
        @endforeach
    </ul>
    <hr>

    <h3>تسوق الفن</h3>
    <ul>
        @foreach($accessories as $accessory)
            <li>
                <a href="{{ route('accessory.show', ['slug' => $accessory->slug]) }}"><h3>{{ $accessory->name }}</h3></a>
                <a href="{{ route('accessory', ['category' => $accessory->category->slug]) }}"><small>{{ $accessory->category->name }}</small></a>
                <hr>
            </li>
        @endforeach
    </ul>
    <hr>

    <h3>الاعمال</h3>
    <ul>
        @foreach($works as $work)
            <li><a href="{{ route('artists.show', ['slug' => $work->artist->username]) }}">{{ $work->name }}</a></li>
        @endforeach
    </ul>
    <hr>


@endsection
@extends('theme.partials.master')

@section('pageTitle', $gallery->info->display_name)

@section('content')

    <h3>{{ $gallery->info->display_name }}</h3>
    <small>{{ $gallery->email }}</small>

    <hr>

    <p>{{ $gallery->name }}</p>
    <p>{{ $gallery->country->country_ar }}</p>
    <p>{{ $gallery->city->ar }}</p>

    <hr>

    <h4>التصنيفات</h4>
    <ul>
        @php $categories = $gallery->info->categories @endphp
        @if(!$categories->isEmpty())
            @foreach($categories as $category)
                <li>
                    <a href="{{ route('galleries', ['category' => $category->slug]) }}">
                        <h5>{{ $category->name }}</h5>
                    </a>
                </li>
            @endforeach
        @else
            <div class="alert alert-warning">لا يوجد تصنيفات</div>
        @endif
    </ul>

    <hr>

    <h4>فعاليات المعرض</h4>
    <ul>
        @php $events = $gallery->events @endphp
        @foreach($events as $item)
            <li><a href="{{ route('events.show', ['slug' => $item->slug]) }}"><h5>{{ $item->title }}</h5></a></li>
            <h6>الفنانين المشاركين</h6>
            @if(!$item->artists->isEmpty())
                <ul>
                    @foreach($item->artists as $artist)
                        <li><a href="{{ route('artists.show', ['slug' => $artist->username]) }}">{{ $artist->info->display_name }}</a></li>
                    @endforeach
                </ul>
            @else
                <div class="alert alert-warning">لا يوجد فنانين مشاركين</div>
            @endif
            <hr>
        @endforeach
    </ul>

@endsection
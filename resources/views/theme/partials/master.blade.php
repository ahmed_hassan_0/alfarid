<!doctype html>
<html lang="ar">
    <head>
        <!-- Required meta tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <meta name="keywords" content="{{ _setting('theme-keywords') }}">
        <meta name="description" content="{{ _setting('theme-description') }}">
        <meta name="author" content="Cloudsoft-it">

        {{-- my meta tags --}}
        <meta name="assets-path" content="{{ asset('storage/') }}"/>
        <meta name="base-url" content="{{ url('/') }}"/>
        <meta name="loading" content="{{ asset('dashboard/images/loader.gif') }}"/>
        <meta name="main-color" content="{{ _setting('theme-color') ?? '#07926d' }}"/>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">

        <!-- Icon Logo -->
        <link rel="shortcut icon" href="{{ asset('storage/' . _setting('theme-icon_image')) }}" type="image/png">

        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">

        <!-- Css Compiler -->
        <link rel="stylesheet" href="{{ url('css/app.css') }}">

        <!-- My Css -->
        <link rel="stylesheet" href="{{ url('theme/css/main.css') }}">

        @stack('css')

        @if(isset($pageTitle))
            <title> {{ _setting('theme-name', 'فنون') }} - {{ $pageTitle }}</title>
        @else
            <title> {{ _setting('theme-name', 'فنون') }} - @yield('pageTitle')</title>
        @endif

        <style>

            @php $mainColor = _setting('theme-color') ?? '#07926d' @endphp

            /* Custom Style */
            .main-color {
                color: {{  $mainColor }} !important;
            }

            .v-application .main--text {
                color: {{  $mainColor }} !important;
                caret-color: {{  $mainColor }} !important;
            }

            h1,h2,h3 {
                color: {{  $mainColor }};
            }

            .navbar-brand{
                color: {{  $mainColor }}  !important;
            }

            a {
                color: {{  $mainColor }};
            }

            .text-main-color {
                color: {{ $mainColor }} !important;
            }

            .main-background-color {
                background-color: {{ $mainColor }} !important;
            }

            .main-border-color {
                border-color: {{ $mainColor }} !important;
            }

            .btn-primary {
                background-color: {{ $mainColor }} !important;
                border-color: {{ $mainColor }} !important;
            }

            .page-title {
                color: {{ $mainColor }} !important;
            }

            ._nav-tabs li.active > a {
                background-color: {{ $mainColor }} !important;
            }

            ._nav-tabs li.active > a:hover, .hover:hover {
                background-color: {{ $mainColor }} !important;
            }

            ._nav-tabs li.active a:hover {
                color: #fff;
                background-color: {{ $mainColor }} !important;
                border-color: transparent transparent {{ $mainColor }};
            }

            /* END Custom Style */
        </style>

        <script>
            window.storage_link = document.querySelector("meta[name=assets-path]").getAttribute('content');
            window.base_url = document.querySelector("meta[name=base-url]").getAttribute('content');
            window.loading = document.querySelector("meta[name=loading]").getAttribute('content');
            window.main_color = document.querySelector("meta[name=main-color]").getAttribute('content');
        </script>

    </head>
    <body>

        <!-- Include Navbar -->
        @include('theme.partials.nav')

        <div id="app">

            <div class="container">

                @yield('content')

            </div>

        </div>

        <!-- Footer -->
        @include('theme.partials.footer')

        <!-- Include js scripts -->
        @include('theme.partials.scripts')

        @stack('js')

    </body>
</html>
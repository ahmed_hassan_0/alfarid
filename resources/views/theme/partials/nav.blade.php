<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('home') }}">فنوان</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ isRoute('artists') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('artists') }}"> فنانين @if(isRoute('artists'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('galleries') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('galleries') }}"> معارض @if(isRoute('galleries'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('events') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('events') }}"> الفعاليات الفنية @if(isRoute('events'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('studio') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('studio') }}"> الاستوديو @if(isRoute('studio'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('accessory') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('accessory') }}"> تسوق الفن @if(isRoute('accessory'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('news') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('news') }}"> الاخبار @if(isRoute('news'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('articles') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('articles') }}"> مقالات الفنانين @if(isRoute('articles'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('common-question') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('common-question') }}"> الاسئلة الشائعة @if(isRoute('common-question'))<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item {{ isRoute('contact-us') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('contact-us') }}"> تواصل معنا @if(isRoute('contact-us'))<span class="sr-only">(current)</span>@endif</a>
            </li>
        </ul>
    </span>
    </div>
</nav>
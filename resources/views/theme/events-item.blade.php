@extends('theme.partials.master')

@section('pageTitle', $event->title)

@section('content')

    <img src="{{ asset('storage/' . $event->image) }}" class="img-fluid" style="height: 300px; width: 100%" alt="{{ $event->title }}">

    <h3>{{ $event->gallery->info->display_name }}</h3>

    <h3>{{ $event->title }}</h3>
    <small>{{ $event->display_start_at->format('Y-m-d g:i A') }}</small>
    <br>
    <small>{{ $event->display_end_at->format('Y-m-d g:i A') }}</small>

    <hr>

    <p class="text-{{ $event->status['class'] }}">{{ $event->status['label'] }} - {{ $event->status['diff_time'] }}</p>

    <hr>

    <p>{!! $event->desc !!}</p>

    <hr>

    <h4>الفنانين</h4>
    <ul>
        @php $artists = $event->artists @endphp
        @if(!$artists->isEmpty())
            @foreach($artists as $artist)
                <li>
                    <a href="{{ route('artists.show', ['slug' => $artist->username]) }}">
                        <h5>{{ $artist->info->display_name }}</h5>
                    </a>
                </li>
            @endforeach
        @else
            <div class="alert alert-warning">لا يوجد فنانين مشتركين</div>
        @endif
    </ul>

@endsection
@extends('theme.partials.master')

@section('pageTitle', 'الفنانين')

@section('content')
    <h2>الفنانين</h2>

    <hr>

    <ul>
        @foreach($artists->items() as $item)
            <li><a href="{{ route('artists.show', ['slug' => $item->username]) }}"><h3>{{ $item->info->display_name }}</h3></a></li>
        @endforeach
    </ul>

@endsection
@extends('theme.partials.master')

@section('pageTitle', 'الفعاليات الفنية')

@section('content')
    <h2>الفعاليات الفنية</h2>
    <ul>
        @foreach($events as $event)
            <li>
                <a href="{{ route('events.show', ['slug' => $event->slug]) }}">
                    <h3>{{ $event->title }}</h3>
                </a>
                <p>{{ $event->display_start_at->format('Y-m-d g:i A') }} <br> {{ $event->display_end_at->format('Y-m-d g:i A') }}</p>
                <small class="text-{{ $event->status['class'] }}">{{ $event->status['label'] }}</small>
            </li>
        @endforeach
    </ul>
@endsection
@extends('theme.partials.master')

@section('content')

    <h1>{{ $pageTitle }}</h1>

    @php $items = $items instanceof \Illuminate\Pagination\LengthAwarePaginator ? $items->items() : $items @endphp

    <div class="container-fluid">
        <div class="row">
            @foreach($items as $item)
                <div class="col-md-4 col-sm-12 px-0">
                    @php $type = $item->type @endphp
                    <a href="{{ route("$type.show", ['slug' => $item->slug]) }}">
                        <app-new-item :item="{{ $item }}"></app-new-item>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator)
        {{ $item->links() }}
    @endif

@endsection
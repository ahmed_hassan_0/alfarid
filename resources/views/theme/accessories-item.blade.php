@extends('theme.partials.master')

@section('pageTitle', $accessory->name)

@section('content')

    @php $images = $accessory->images @endphp
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($images as $image)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <img style="height: 300px; width: 100%" src="{{ asset('storage/accessories/original/' . $image->image) }}" class="d-block img-fluid">
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <h3>{{ $accessory->name }}</h3>
    <small>{{ $accessory->created_at }}</small>

    <hr>

    <h4>التصنيفات</h4>
    <p><a href="{{ route('accessory', ['category' => $accessory->category->slug]) }}">{{ $accessory->category->name }}</a></p>

@endsection
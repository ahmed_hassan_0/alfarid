@extends('theme.partials.master')

@section('pageTitle', $item->title)

@section('content')
    <img src="{{ asset('storage/' . $item->image) }}" class="img-fluid" style="height: 300px; width: 100%" alt="{{ $item->title }}">
    <div class="col-12">
        <h3>{{ $item->title }}</h3>
        @if($item->isArticle())
            <a href="{{ route('articles') . '?author=' . optional($item->artistUser)->username }}">{{ optional($item->artist)->display_name }}</a>
        @endif
        <br>
        <small>{{ $item->created_at }}</small>
        <p>{!! $item->body !!}</p>
    </div>
@endsection
@extends('theme.partials.master')

@section('pageTitle', $artist->info->display_name)

@section('content')

    <h3>{{ $artist->info->display_name }}</h3>
    <small>{{ $artist->email }}</small>

    <hr>

    <p>{{ $artist->name }}</p>
    <p>{{ $artist->country->country_arNationality }}</p>
    <p>{{ $artist->country->country_ar }}</p>
    <p>{{ $artist->city->ar }}</p>

    <hr>

    <h4>التصنيفات</h4>
    <ul>
        @php $categories = $artist->info->categories @endphp
        @if(!$categories->isEmpty())
            @foreach($categories as $category)
                <li>
                    <a href="{{ route('artists', ['category' => $category->slug]) }}">
                        <h5>{{ $category->name }}</h5>
                    </a>
                </li>
            @endforeach
        @else
            <div class="alert alert-warning">لا يوجد تصنيفات</div>
        @endif
    </ul>

    <hr>

    <h4>الفعاليات</h4>
    <ul>
        @foreach($artist->artistEvent as $item)
            <li><a href="{{ route('galleries.show', ['slug' => $item->gallery->username]) }}">{{ $item->gallery->info->display_name }}</a> : {{ $item->title }}</li>
        @endforeach
    </ul>

@endsection
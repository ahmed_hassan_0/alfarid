@extends('theme.partials.master')

@section('pageTitle', 'المعارض')

@section('content')
    <h2>المعارض</h2>

    <hr>

    <ul>
        @foreach($galleries->items() as $item)
            <li><a href="{{ route('galleries.show', ['slug' => $item->username]) }}"><h3>{{ $item->info->display_name }}</h3></a></li>
        @endforeach
    </ul>

@endsection
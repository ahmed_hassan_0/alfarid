@extends('theme.partials.master')

@section('pageTitle', 'الاسئلة الشائعة')

@section('content')

    <h1>الاسئلة الشائعة</h1>

    <app-common-questions :questions="{{ json_encode($questions->items()) }}"></app-common-questions>

    {{ $questions->links() }}

@endsection
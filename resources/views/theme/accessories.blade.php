@extends('theme.partials.master')

@section('pageTitle', 'تسوق الفن')

@section('content')

    <h1>تسوق الفن</h1>

    <hr>

    <ul>
        @foreach($accessories->items() as $item)
            <li>
                <a href="{{ route('accessory.show', ['slug' => $item->slug]) }}"><h3>{{ $item->name }}</h3></a>
                <small>التصنيف</small><br>
                <a href="{{ route('accessory', ['category' => $item->category->slug]) }}"><small>{{ $item->category->name }}</small></a>
                <hr>
            </li>
        @endforeach
    </ul>

@endsection
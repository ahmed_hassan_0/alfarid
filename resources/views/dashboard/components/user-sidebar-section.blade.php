<div class="panel panel panel-bordered panel-warning">
    <div class="panel-body">
        <div class="form-group">
            @if(isset($dataTypeContent->avatar))
                <img src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}" style="width: 144px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
            @endif
            <input type="file" data-name="avatar" name="avatar">
        </div>
        <div class="form-group">
            <label for="country_id">الدولة</label>
            <select required onchange='iChange(this.value, "select[name=city_id]", @json(old('city_id', $dataTypeContent->city_id)))' class="form-control select2" id="country_id" name="country_id">
                <?php
                $countries = \App\Helpers\Utilities::getCountries();
                ?>
                @foreach ($countries as $country)
                    <option value="{{ $country->id }}"
                            {{ ($country->id == old('country_id', $dataTypeContent->country_id) ? 'selected' : '') }}
                        {{ $country->country_code == 'SA' ? 'selected' : '' }}
                    >
                        {{ $country->country_ar }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="city_id">المدينة</label>
            <select data-placement="المدينة" required class="form-control select2" id="city_id" name="city_id"></select>
        </div>
        <div class="form-group">
            <label for="area">الحي</label>
            <input required type="text" class="form-control" id="area" name="area" value="{{ old('area', $dataTypeContent->area ?? '') }}">
        </div>
    </div>
</div>


@push('javascript')
    <script>
        var countries = @json($countries)

        function iChange(val, target, selectId = null) {
            var country = countries.find(function (country) { return country.id == val });
            var cities = country.cities;
            var options = '';
            for (var i = 0; i < cities.length; i++) {
                options += '<option value="'+ cities[i].id +'">';
                options += cities[i].ar
                options += '</option>';
            }
            $(target).html(options);
            if (selectId) {
                $(target).val(selectId);
            }
        }

        $(function () {
            $('select[name=country_id]').trigger('change');
        })

    </script>
@endpush
<div class="form-group">
    <label for="name">الاسم الفريد</label>
    <br>
    <small>يجب ان يكون غير مكرر</small>
    <input required type="text" class="form-control" id="username" name="username"
           value="{{ old('username', $dataTypeContent->username ?? '') }}">
</div>



@push('javascript')
    <script>

        $(function () {
            $('input[name={{ isset($targetName) ? $targetName : 'name' }}]').keyup(function () {
                $('input[name=username]').val(convertToSlug($(this).val()));
            })
            $('input[name=name]').trigger('keyup');
        })

    </script>
@endpush
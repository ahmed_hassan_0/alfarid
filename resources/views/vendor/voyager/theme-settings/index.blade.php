@extends('voyager::master')

@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <div class="page-content container-fluid">

            <div class="row">

                <div class="col-md-6 col-sm-12">

                    <div class="col-md-12 pull-right">
                        <form action="{{ route('voyager.settings.store') }}?fields=theme-icon_image-ar,theme-icon_image-en,theme-color,theme-font-color,theme-paginate-count" method="POST" class="ajax-form">
                            <div class="panel panel-body">
                                <div class="loading" style="background-image: url({{ asset('dashboard/images/loader.gif') }})"></div>
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="main-color section-title" style="margin-right: 0">{{ __('main.theme-settings') }}</h3>
                                    <button class="btn btn-primary btn-collapse" type="button" data-toggle="collapse" data-target="#b" aria-expanded="true" aria-controls="b">
                                        <i class="voyager-sort-desc"></i>
                                    </button>
                                </div>

                                <div id="b" class="collapse in">

                                    <div class="form-group">
                                        <label>{{ __('main.icon-theme-ar') }}</label>
                                        @php $icon_image = _setting('theme-icon_image-ar'); @endphp
                                        @if($icon_image)
                                            <br>
                                            <img id="theme-icon_image-ar" style="width: 150px;" src="{{ asset("storage/$icon_image") }}" title="{{ __('main.icon-theme-ar') }}">
                                            <br>
                                        @endif
                                        <input type="file" name="theme-icon_image-ar" class="preview" data-preview-target="theme-icon_image-ar">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.icon-theme-en') }}</label>
                                        @php $icon_image_en = _setting('theme-icon_image-en'); @endphp
                                        @if($icon_image_en)
                                            <br>
                                            <img id="theme-icon_image-en" style="width: 150px;" src="{{ asset("storage/$icon_image_en") }}" title="{{ __('main.icon-theme-en') }}">
                                            <br>
                                        @endif
                                        <input type="file" name="theme-icon_image-en" class="preview" data-preview-target="theme-icon_image-en">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.color') }}</label>
                                        <br>
                                        <input type="color" class="form-control" name="theme-color" value="{{ _setting('theme-color', '#07926d') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.font-color') }}</label>
                                        <br>
                                        <input type="color" class="form-control" name="theme-font-color" value="{{ _setting('theme-font-color', '#888888') }}">
                                    </div>

                                    {{--<div class="form-group">--}}
                                        {{--<label>{{ __('main.paginate_count') }}</label>--}}
                                        {{--<br>--}}
                                        {{--<input type="number" min="5" name="theme-paginate-count" class="form-control" value="{{ _setting('theme-paginate-count', 5) }}">--}}
                                    {{--</div>--}}

                                    <button class="btn btn-primary" type="submit" style="margin-top: 15px; ">{{ __('main.buttons.save') }}</button>

                                </div>

                            </div>
                        </form>
                    </div>

                    <div class="col-md-12 pull-right">
                        <form action="{{ route('voyager.settings.store') }}?fields=theme-name-ar,theme-keywords-ar,theme-description-ar,theme-name-en,theme-keywords-en,theme-description-en" method="POST" class="ajax-form">
                            <div class="panel panel-body">
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="main-color section-title" style="margin-right: 0">{{ __('main.website-data') }}</h3>
                                    <button class="btn btn-primary btn-collapse" type="button" data-toggle="collapse" data-target="#c" aria-expanded="true" aria-controls="c">
                                        <i class="voyager-sort-desc"></i>
                                    </button>
                                </div>

                                <div id="c" class="collapse in">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs _nav-tabs" role="tablist" style="margin-bottom: 15px">
                                        <li role="presentation" class="{{ app()->getLocale() == 'ar' ? 'active' : '' }}"><a href="#ar" aria-controls="home" role="tab" data-toggle="tab">{{ __('main.arabic') }}</a></li>
                                        <li role="presentation" class="{{ app()->getLocale() == 'en' ? 'active' : '' }}"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">{{ __('main.english') }}</a></li>
                                    </ul>

                                    <div class="tab-content" style="margin: -20px;">

                                        <div id="ar" role="tabpanel" class="tab-pane {{ app()->getLocale() == 'ar' ? 'active' : '' }}">
                                            <div class="form-group">
                                                <label>{{ __('main.website-name') }}</label>
                                                <input class="form-control" type="text" name="theme-name-ar" value="{{ _setting('theme-name-ar') }}">
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('main.website-keywords') }}</label>
                                                <input class="form-control" type="text" name="theme-keywords-ar" value="{{ _setting('theme-keywords-ar') }}">
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('main.website-desc') }}</label>
                                                <textarea rows="4" class="form-control" name="theme-description-ar">{{ _setting('theme-description-ar') }}</textarea>
                                            </div>
                                        </div>

                                        <div id="en" role="tabpanel" class="tab-pane {{ app()->getLocale() == 'en' ? 'active' : '' }}">
                                            <div class="form-group">
                                                <label>{{ __('main.website-name') }}</label>
                                                <input class="form-control" type="text" name="theme-name-en" value="{{ _setting('theme-name-en') }}">
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('main.website-keywords') }}</label>
                                                <input class="form-control" type="text" name="theme-keywords-en" value="{{ _setting('theme-keywords-en') }}">
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('main.website-desc') }}</label>
                                                <textarea rows="4" class="form-control" name="theme-description-en">{{ _setting('theme-description-en') }}</textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <button class="btn btn-primary" type="submit" style="margin-top: 15px; ">{{ __('main.buttons.save') }}</button>

                                </div>

                            </div>
                        </form>
                    </div>

                </div>

                <div class="col-md-6 col-sm-12">

                    <div class="col-md-12 pull-right">
                        <form action="{{ route('voyager.settings.store') }}?fields=email,phone,address,facebook,twitter,telegram,youtube,snapchat,instagram" method="POST" class="ajax-form">
                            <div class="panel panel-body">
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="main-color section-title"  style="margin-right: 0">{{ __('main.contact-us') }}</h3>
                                    <button class="btn btn-primary btn-collapse" type="button" data-toggle="collapse" data-target="#t" aria-expanded="true" aria-controls="t">
                                        <i class="voyager-sort-desc"></i>
                                    </button>
                                </div>

                                <div id="t" class="collapse in">

                                    <div class="form-group">
                                        <label>{{ __('main.email') }}</label>
                                        <input class="form-control" type="email" name="email" value="{{ _setting('email') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.phone') }}</label>
                                        <input class="form-control" type="text" name="phone" value="{{ _setting('phone') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.address') }}</label>
                                        <input class="form-control" type="text" name="address" value="{{ _setting('address') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.facebook') }}</label>
                                        <input class="form-control" type="text" name="facebook" value="{{ _setting('facebook') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.twitter') }}</label>
                                        <input class="form-control" type="text" name="twitter" value="{{ _setting('twitter') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.telegram') }}</label>
                                        <input class="form-control" type="text" name="telegram" value="{{ _setting('telegram') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.youtube') }}</label>
                                        <input class="form-control" type="text" name="youtube" value="{{ _setting('youtube') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.snapchat') }}</label>
                                        <input class="form-control" type="text" name="snapchat" value="{{ _setting('snapchat') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.instagram') }}</label>
                                        <input class="form-control" type="text" name="instagram" value="{{ _setting('instagram') }}">
                                    </div>

                                    <button class="btn btn-primary" type="submit" style="margin-top: 15px; ">{{ __('main.buttons.save') }}</button>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>


        </div>
    </div>
@stop

@section('javascript')

@stop

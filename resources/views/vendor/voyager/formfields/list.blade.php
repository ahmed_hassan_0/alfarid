<div id="rows" class="ar-row {{ app()->getLocale() == 'en' ? 'display-hidden' : '' }}">
    {{-- rows --}}
    <div id="container-rows" class="container-rows">

        @if (isset($dataTypeContent->id) && json_decode($dataTypeContent->features) != null)
            @php $features = json_decode($dataTypeContent->features, true) @endphp
            @foreach(_arrayGet($features, 'ar', [])  as $feature_ar)
                <div class="form-group col-md-12 row" style="margin-bottom: 0; padding-bottom: 0">
                    <div class="col-md-8" style="margin-bottom: 0">
                        <input type="text" class="form-control" name="{{ $row->field }}[ar][]" placeholder="{{ $row->display_name }}" value="{{ $feature_ar }}">
                    </div>
                    <button class="btn btn-danger delete-row" style="margin: 12px;" type="button"><i class="voyager-x"></i></button>
                </div>
            @endforeach
        @endif

    </div>
    <div class="text-center">
        <button class="btn btn-success" type="button" id="btn-rows-ar">{{ __('main.add_new_field') }}</button>
    </div>
</div>

<div id="rows" class="en-row {{ app()->getLocale() == 'ar' ? 'display-hidden' : '' }}">
    {{-- rows --}}
    <div id="container-rows" class="container-rows">

        @if (isset($dataTypeContent->id) && json_decode($dataTypeContent->features) != null)
            @php $features = json_decode($dataTypeContent->features, true) @endphp
            @foreach(_arrayGet($features, 'en', []) as $feature_en)
                <div class="form-group col-md-12 row" style="margin-bottom: 0; padding-bottom: 0">
                    <div class="col-md-8" style="margin-bottom: 0">
                        <input type="text" class="form-control" name="{{ $row->field }}[en][]" placeholder="{{ $row->display_name }}" value="{{ $feature_en }}">
                    </div>
                    <button class="btn btn-danger delete-row" style="margin: 12px;" type="button"><i class="voyager-x"></i></button>
                </div>
            @endforeach
        @endif

    </div>
    <div class="text-center">
        <button class="btn btn-success" type="button" id="btn-rows-en">{{ __('main.add_new_field') }}</button>
    </div>
</div>

@push('javascript')

    <script type="text/template" id="row-template-ar">
        <div class="form-group col-md-12 row" style="margin-bottom: 0; padding-bottom: 0">
            <div class="col-md-8" style="margin-bottom: 0">
                <input type="text" class="form-control" name="{{ $row->field }}[ar][]" placeholder="{{ $row->display_name }}" value="">
            </div>
            <button class="btn btn-danger delete-row" style="margin: 12px;" type="button"><i class="voyager-x"></i></button>
        </div>
    </script>

    <script type="text/template" id="row-template-en">
        <div class="form-group col-md-12 row" style="margin-bottom: 0; padding-bottom: 0">
            <div class="col-md-8" style="margin-bottom: 0">
                <input type="text" class="form-control" name="{{ $row->field }}[en][]" placeholder="{{ $row->display_name }}" value="">
            </div>
            <button class="btn btn-danger delete-row" style="margin: 12px;" type="button"><i class="voyager-x"></i></button>
        </div>
    </script>


    <script>

        $(function() {

            $('#btn-rows-ar').on('click', function() {

                $(this).closest('.ar-row').find('#container-rows').append($('#row-template-ar').html());

            })

            $('#btn-rows-en').on('click', function() {

                $(this).closest('.en-row').find('#container-rows').append($('#row-template-en').html());

            })

            @if(!isset($dataTypeContent->id))
                $('#btn-rows-ar').trigger('click');
                $('#btn-rows-en').trigger('click');
            @endif

            $(document).on('click', '.delete-row', function() {
                $(this).closest('.row').remove();
            });

            $('input[name=i18n_selector]').change(function () {
                if ($(this).attr('id') == 'ar') {
                    $('.ar-row').css('display', 'block');
                    $('.en-row').css('display', 'none');
                }else {
                    $('.ar-row').css('display', 'none');
                    $('.en-row').css('display', 'block');
                }
            });

        })

    </script>


@endpush
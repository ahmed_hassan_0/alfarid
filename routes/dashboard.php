<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */


Route::group(['prefix' => 'admin'], function () {

    \TCG\Voyager\Facades\Voyager::routes();

    //guest routes
    Route::group(['middleware' => 'guest', 'as' => 'voyager.'], function () {
        Route::get('forgot-password', 'Dashboard\\Auth\\ForgotPasswordController@showLinkRequestForm');
        Route::post('send-reset-link', 'Dashboard\\Auth\\ForgotPasswordController@sendResetLinkEmail');
        Route::get('reset-password', 'Dashboard\\Auth\\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('reset-password', 'Dashboard\\Auth\\ResetPasswordController@reset');
    });

    Route::group(['namespace' => 'Dashboard', 'as' => 'voyager.', 'middleware' => ['auth']], function () {

        //theme settings
        Route::get('theme-settings', 'SettingsController@themeSettings')->name('theme-settings.index');

        //admin settings
        Route::post('settings', 'SettingsController@adminSettings')->name('settings.store');

        //remove work image with ajax
        Route::post('work-remove-image', 'WorkController@removeImage')->name('works.delete-image');

    });

});
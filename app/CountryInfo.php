<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryInfo extends Model
{
    protected $table = 'country_info';
}

<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */

namespace App\FormFields;
use TCG\Voyager\FormFields\AbstractHandler;

class ListFormField extends AbstractHandler
{
    protected $codename = 'list';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.list', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}
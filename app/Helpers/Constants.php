<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */


namespace App\Helpers;


class Roles {
    const Admin = 'admin';
    const Artist = 'artist';
    const User = 'user';
    const Gallery = 'gallery';
}


class Path {
    const DS = DIRECTORY_SEPARATOR;
    const WorksFolder = "works";
}


class Config {
    const LogoWidth = 100;
    const LogoHeight = 100;
    const ThumbnailHeight = 300;
    const ThumbnailsWidth = 350;
}


class EventMode {
    const Waiting = 'waiting';
    const Done = 'done';
    const Active = 'active';
    const Cancel = 'cancel';


    public static function getModes()
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);
        return $reflectionClass->getConstants();
    }
}


class OrderBY {
    const CommonQuestion = 'asc';
    const SlidersOrder = 'asc';
    const FeatureOrder = 'asc';
    const Events = 'asc';
    const Artists = 'asc';
    const Galleries = 'asc';
    const Articles = 'asc';
    const News = 'asc';
    const Accessories = 'asc';
}


class Limit {
    const HomeEvents = 5;
    const HomeArtists = 10;
    const HomeWorks = 10;
    const StudioItem = 10;
}


class Paginate {
    const CommonQuestion = 10;
    const News = 10;
    const Articles = 10;
    const Artists = 10;
    const Galleries = 10;
    const Events = 10;
    const Accessories = 10;
}


class Cache {
    const Seconds = 30;
}

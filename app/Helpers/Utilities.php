<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */

namespace App\Helpers;


use App\Accessory;
use App\ArtisticInfo;
use App\Category;
use App\Country;
use App\User;
use App\Work;
use App\Image;
use function GuzzleHttp\Psr7\uri_for;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\Collection;

class Utilities
{

    private static $settings = null;


    /**
     * Get Countries With Cities
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getCountries()
    {
        return Country::with('cities')->get();
    }


    /**
     * Get Setting
     *
     * @param $key
     * @param $table
     * @return null
     */
    public static function setting($key, $table)
    {
        if (self::$settings == null) {
            self::$settings = \Illuminate\Support\Facades\DB::table($table)->get();
        }

        $val = _objectGet(self::$settings->where('key', $key)->first(), 'value');

        if (is_null($val)) {
            $key = $key . '-' . app()->getLocale();
            $val = _objectGet(self::$settings->where('key', $key)->first(), 'value');
        }

        return $val;
    }


    /**
     * User Roles admin || user || artist || gallery
     *
     * @param $slug
     * @param null $id
     * @return array
     */
    public static function userRoles($slug, $id = null)
    {
        $rules = [
            'name'                  => 'required',
            'email'                 => ['required', 'email', '_email'],
        ];

        $uniqueEmail = Rule::unique('users', 'email');
        if ($id) {
            //update
            $uniqueEmail = $uniqueEmail->ignore($id);
            if (request()->filled('password')) {
                $rules['password'] = 'min:6';
            }
        }else {
            //create
            $rules['password'] = 'required|min:6';
        }

        array_push($rules['email'], $uniqueEmail);

        return $rules;
    }


    /**
     * Get Categories
     *
     * @param $order
     * @return mixed
     */
    public static function categories($order = 'asc')
    {
        return Category::orderBy('order', $order)->get();
    }


    /**
     * Update Setting
     *
     * @param $column
     * @param $updateValues
     * @return int
     */
    public static function updateSetting($column, $updateValues)
    {
        $exists = DB::table('settings')->where('key', $column)->count();
        if ($exists) {
            return DB::table('settings')->where('key', $column)->update($updateValues);
        }
        return DB::table('settings')->insert(array_merge($updateValues, ['key' => $column]));
    }


    /**
     * Generate Main Directories
     *
     * @param $folder_name
     * @return bool
     */
    public static function workDirectories($folder_name)
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        $folder_path = $worksFolder . $DS . $folder_name;
        if (!Storage::disk('public')->exists($folder_path)) {
            Storage::disk('public')->makeDirectory($folder_path . $DS . "original");
            Storage::disk('public')->makeDirectory($folder_path . $DS . "front");
            Storage::disk('public')->makeDirectory($folder_path . $DS . "thumbnails");
        }
        return true;
    }


    /**
     * Rename Directory
     *
     * @param $old_name
     * @param $new_name
     * @return bool
     */
    public static function renameDirectory($old_name, $new_name)
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        if (!Storage::disk('public')->exists("$worksFolder/$old_name")) {
            return Utilities::workDirectories($new_name);
        }

        $work_path = storage_path("app".$DS."public".$DS.$worksFolder.$DS);
        return rename($work_path.$old_name, $work_path . $new_name);
    }


    /**
     * Clear Artist Directories
     *
     * @param $folder_name
     * @return bool
     */
    public static function clearDirectory($folder_name)
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        return Storage::disk('public')->deleteDirectory($worksFolder . $DS . $folder_name);
    }


    /**
     * Create Work Directory
     *
     * @param $work
     * @return array
     */
    public static function createWorkDirectory(Work $work)
    {
        [$original, $front, $thumbnails] = self::getWorkDirectory($work);

        Storage::disk('public')->makeDirectory($original);
        Storage::disk('public')->makeDirectory($front);
        Storage::disk('public')->makeDirectory($thumbnails);

        return [$original, $front, $thumbnails];
    }


    /**
     * Get Work Directory
     *
     * @param Work $work
     * @return array
     */
    public static function getWorkDirectory(Work $work)
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        $artist = User::findOrFail($work->artist_id);
        $folder_path = $worksFolder . $DS . $artist->username . '-' . $artist->id;
        $original = $folder_path . $DS . "original" . $DS . $work->slug;
        $front = $folder_path . $DS . "front" . $DS . $work->slug;
        $thumbnails = $folder_path . $DS . "thumbnails" . $DS . $work->slug;

        return [$original, $front, $thumbnails];
    }


    /**
     * Get Absolute Artist Directories
     *
     * @param User $user
     * @return array
     */
    public static function getArtistDirectories(User $user)
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        $folder_path = $worksFolder . $DS . $user->username . '-' . $user->id;
        $original = $folder_path . $DS . "original";
        $front = $folder_path . $DS . "front";
        $thumbnails = $folder_path . $DS . "thumbnails";

        return [$original, $front, $thumbnails];
    }


    /**
     * Delete Work Directory
     *
     * @param $work
     * @param $type
     * @return bool
     */
    public static function deleteWorkDirectory(Work $work, $type = ['original', 'front', 'thumbnails'])
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        $artist = User::findOrFail($work->artist_id);
        $folder_path = $worksFolder . $DS . $artist->username . '-' . $artist->id;
        foreach ($type as $item) {
            $path = $folder_path . $DS . $item . $DS . $work->slug;
            Storage::disk('public')->deleteDirectory($path);
        }

        return true;
    }


    /**
     * Delete Type [original || front || thumbnails] Directory
     *
     * @param User $artist
     * @param array $type
     * @return bool
     */
    public static function deleteTypeDirectory(User $artist, $type = ['front', 'thumbnails'])
    {
        $worksFolder = Path::WorksFolder;
        $DS = Path::DS;
        $folder_path = $worksFolder . $DS . $artist->username . '-' . $artist->id;
        foreach ($type as $item) {
            $path = $folder_path . $DS . $item;
            Storage::disk('public')->deleteDirectory($path);
        }

        return true;
    }


    /**
     * Upload Work Images With Logo
     *
     * @param ArtisticInfo $artist
     * @param $from
     * @param $to
     * @param $new_if_exist
     * @return bool
     */
    public static function uploadWorkImages(ArtisticInfo $artist, $from, $to, $new_if_exist = false)
    {
        $DS = Path::DS;
        $logo = storage_path('app' . $DS . 'public' . $DS . $artist->digital_signature);
        $main_path = storage_path('app' . Path::DS . 'public' . Path::DS);
        $margin_right = $artist->digital_signature_position_x == 'right' ? 10 : -10;
        $margin_bottom = $artist->digital_signature_position_y == 'bottom' ? 10 : -10;

        return _addLogoToImage(
            $main_path . $from,
             $logo,
            $main_path . $to,
            Config::LogoWidth,
            Config::LogoHeight,
            $margin_right,
            $margin_bottom,
            $new_if_exist
        );
    }


    /**
     * Create Thumbnails
     *
     * @param $from_dir
     * @param $to_dir
     * @param bool $new_if_exists
     * @return bool
     */
    public static function createThumbnails($from_dir, $to_dir, $new_if_exists = false)
    {
        $thumbnails_height = Config::ThumbnailHeight;
        $thumbnails_width = Config::ThumbnailsWidth;
        $DS = Path::DS;
        $from_dir = storage_path('app' . $DS . 'public' . $DS . $from_dir);
        $to_dir = storage_path('app' . $DS . 'public' . $DS . $to_dir);

        return _makeThumbnails($from_dir, $to_dir, $thumbnails_width, $thumbnails_height, null, $new_if_exists);
    }


    /**
     * Store images to db and uploaded it in original dir
     *
     * @param Work $work
     * @param array $images
     * @param $original_path
     * @return bool
     */
    public static function storeWorkImagesToDB(Work $work, array $images, $original_path)
    {
        $insertImages = [];
        $max_order = $work->images()->max('order') ?? 1;

        foreach ($images as $key => $image) {

            $image_name = bin2hex(openssl_random_pseudo_bytes(16)) . time() . '.' . $image->extension();

            $image->storeAs($original_path, $image_name,  'public');

            $insertImages[] = [
                'image'   => $image_name,
                'order'   => ($key + $max_order),
                'imageable_id' => $work->id,
                'imageable_type' => Work::class
            ];
        }

        //attach Images with work
        return Image::insert($insertImages);
    }


    /**
     * Delete Image From Work Folder
     *
     * @param $slug
     * @param $image
     * @param $work_slug
     * @param null $type  ===> original || front || null
     * @return bool
     */
    public static function deleteImage($slug, $work_slug, $image, $type = null)
    {
        if ($type == null) {
            $type = ['original', 'front', 'thumbnails'];
        }else {
            $type = [$type];
        }

        $DS = Path::DS;
        $main_path = Path::WorksFolder . $DS . $slug;

        foreach ($type as $f) {
            Storage::disk('public')->delete($main_path . $DS . $f . $DS . $work_slug . $DS . $image);
        }

        return true;
    }

    /**
     * Get Max Order
     *
     * @param $user_id
     * @return int
     */
    public static function getNewWorkOrder($user_id)
    {
        return (int) Work::where('artist_id', $user_id)->max('order') + 1;
    }


    /**
     * Get New Order
     *
     * @param $model
     * @return int
     */
    public static function getNewOrder($model)
    {
        return (int) ($model->max('order') + 1);
    }


    /**
     * Create Accessories Images
     *
     * @param Accessory $accessory
     * @param $images
     * @param $create_if_exists
     * @return bool
     */
    public static function accessoriesImages(Accessory $accessory, $images, $create_if_exists = false)
    {
        $insert_array = [];
        foreach ($images as $key => $image) {
            $image_name = bin2hex(openssl_random_pseudo_bytes(16)) . time() . '.' . $image->extension();
            $image->storeAs('accessories/original', $image_name ,'public');
            $insert_array[] = [
                'image'   => $image_name,
                'order'   => ($key + 1),
                'imageable_id' => $accessory->id,
                'imageable_type' => Accessory::class
            ];
        }

        //store in db
        Image::insert($insert_array);

        //create thumbnails
        Utilities::createThumbnails('accessories'.Path::DS.'original', 'accessories'.Path::DS.'thumbnails', $create_if_exists);

        return true;
    }


    /**
     * Delete Accessory Original and Thumbnails Image
     *
     * @param $image
     * @return bool
     */
    public static function accessoryDeleteImage($image)
    {
        return Storage::disk('public')->delete(["accessories/original/$image", "accessories/thumbnails/$image"]);
    }


    /**
     * Set || get Data From Cache
     *
     * @param $key
     * @param $count_data
     * @param \Closure $closure
     * @param string $store
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function getFromCache($key, $count_data, \Closure $closure, $store = 'file')
    {
        //key count
        $count_name = $key . '_count';

        //check if count current data equal count data in cache
        //we will get data from cache
        if (
            $count_data == Cache::store($store)->get($count_name, false)
            &&
            Cache::store($store)->has($key)
        ) {
            return Cache::store($store)->get($key);
        }

        //else remove old keys if exists
        //and add new count data
        //and set key with new data
        self::removeFromCache($key);

        $second = \App\Helpers\Cache::Seconds;

        //set new count data
        Cache::store($store)->put($count_name, $count_data, $second);

        //set new data and return it
        $data_cached = Cache::store($store)->remember($key, $second, $closure);

        return $data_cached;
    }


    /**
     * Remove From Cache
     *
     * @param $key
     * @param string $store
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function removeFromCache($key, $store = 'file')
    {
        if (Cache::store($store)->has($key)) Cache::store($store)->forget($key);
        if (Cache::store($store)->has($key . '_count')) Cache::store($store)->forget($key . '_count');
    }

}
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param $roles
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $me = Auth::user()->load('role');
        $my_role = $me->role->name;

        //admin,artist
        //OR
        //admin,!artist
        $roles = explode(',', $roles);
        $original_roles = [];

        foreach ($roles as $role) {
            if ($role[0] == '!') {
                $original_name = substr($role, 1);
                abort_if($my_role == $original_name, 403);
            }else {
                array_push($original_roles, $role);
            }
        }

        if (!empty($original_roles)) {
            abort_if(!in_array($my_role, $original_roles), 403);
        }

        return $next($request);
    }
}

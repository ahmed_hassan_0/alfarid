<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */

namespace App\Http\View\Composers;
use App\Page;
use Illuminate\View\View;


class FooterComposer
{
    const PagesOrder = 'asc';

    public function compose(View $view)
    {
        $pages = Page::orderBy('order', self::PagesOrder)->active()->get();
        $social_networks = [];
        $networks  = ['facebook', 'twitter', 'telegram', 'youtube', 'snapchat', 'instagram'];
        foreach ($networks as $network) {
            $social_networks[$network] = _setting($network);
        }

        $view->with(compact('pages', 'social_networks'));
    }

}
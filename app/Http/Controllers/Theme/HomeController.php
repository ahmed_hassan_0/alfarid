<?php

namespace App\Http\Controllers\Theme;

use App\ArtisticFeature;
use App\Event;
use App\Helpers\Limit;
use App\Helpers\OrderBY;
use App\Http\Controllers\Controller;
use App\ThemeSlider;
use App\User;
use App\Work;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function index(Request $request)
    {
        $data = [];
        $data['sliders'] = ThemeSlider::orderBy('order', OrderBY::SlidersOrder)->get();
        $data['features'] = ArtisticFeature::orderBy('order', OrderBY::FeatureOrder)->get();
        $data['latest_events'] = Event::latest()->limit(Limit::HomeEvents)->get();
        $data['latest_artists'] = User::with('info')->latest()->limit(Limit::HomeArtists)->get();
        $data['latest_works'] = Work::with('artist:id,user_id,display_name')
                                    ->with('images')
                                    ->with('tags')
                                    ->latest()
                                    ->limit(Limit::HomeWorks)
                                    ->get();

        return view('theme.home', $data);
    }
}

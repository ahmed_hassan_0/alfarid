<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __invoke(Request $request, $slug)
    {
        $page = Page::where('slug', trim($slug))->active()->first();
        abort_if(!$page, 404);

        return view('theme.page', compact('page'));
    }
}

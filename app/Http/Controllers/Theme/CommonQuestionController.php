<?php

namespace App\Http\Controllers\Theme;

use App\CommonQuestion;
use App\Helpers\OrderBY;
use App\Helpers\Paginate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonQuestionController extends Controller
{
    public function index(Request $request)
    {
        $questions = CommonQuestion::orderBy('order', OrderBY::CommonQuestion)->paginate(Paginate::CommonQuestion);

        return view('theme.common-question', compact('questions'));
    }
}

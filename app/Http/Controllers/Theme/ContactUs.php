<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUs extends Controller
{
    public function index(Request $request)
    {
        $settings = ['email', 'phone', 'address', 'facebook', 'twitter', 'telegram', 'youtube', 'snapchat', 'instagram'];
        $data = [];
        foreach ($settings as $setting) {
            $data[$setting] = _setting($setting);
        }
        return view('theme.contact-us', compact('data'));
    }
}

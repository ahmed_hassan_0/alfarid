<?php

namespace App\Http\Controllers\Theme;

use App\Helpers\Roles;
use App\Http\Controllers\Controller;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Role;

class AuthController extends Controller
{
    //show Login Page
    public function loginForm()
    {
        return view('theme.auth.login');
    }

    //submit login form
    public function loginSubmit(Request $request)
    {
        $validate = Validator::make($request->all(), ['phone' => 'required', 'password' => 'required']);

        if ($validate->fails()) {
            return \response()->json($validate->errors(), 400);
        }

        $user = User::where('phone', $request->phone)->first();

        //check match password
        if (!Hash::check($user->password, $request->password)) {
            return \response()->json(['invalid_data' => 'رقم الهاتف او كلمة المرور غير صحيحة']);
        }

        //check inactive account
        if (!$user->isActive()) {
            //resend verify code
            $this->sendVerificationCode($user);
            return redirect()->route('verify')->with('phone', $user->phone);
        }

        Auth::login($user);

        return redirect()->intended(route('home'));
    }

    //register page
    public function registerForm()
    {
        return view('theme.auth.register');
    }

    //submit register form
    public function registerSubmit(Request $request)
    {
        $attributes = $this->rules($request);

        $attributes['role_id'] = Role::where('name', Roles::User)->value('id');
        if ($request->hasFile('avatar')) {
            $avatar = $request->avatar->store('users');
        }else {
            $avatar = _setting('theme-default-avatar');
        }
        $attributes['avatar'] = $avatar;
        $attributes['code'] = mt_rand(111111, 999999);
        $new_user = User::create($attributes);

        //send verification code
        $this->sendVerificationCode($new_user);

        return redirect()->route('verify')->with(['phone' => $new_user->phone, 'code' => $new_user->code]);
    }

    //register rules
    public function rules(Request $request)
    {
        return $request->validate(
            [
                'name'     => 'required',
                'email'    => 'required|_email|unique:users,email',
                'username' => 'required|unique:users,username',
                'country'  => 'required',
                'city'     => 'required',
                'phone'    => 'required|_phone|unique:users,phone',
                'password' => 'required|confirmed|_password',
                'avatar'   => 'sometimes|image'
            ]
        );
    }

    //verify page
    public function verify(Request $request)
    {
        $phone = $request->session()->get('phone');

        abort_if(!$phone, 404);

        return view('theme.auth.verify', compact('phone'));
    }

    //verify submit form [verify with phone number]
    public function verifySubmit(Request $request)
    {
        $validation = Validator::make($request->all(), ['phone' => 'required', 'code' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 400);
        }

        $user = User::where('phone', $validation['phone'])->where('code', $validation['code'])->first();

        if (!$user) {
            return response()->json(['invalid_data' => 'الكود غير صحيح'], 400);
        }

        $user->code = null;
        $user->save();

        return response()->json(['msg' => 'الكود صحيح']);
    }

    //resend code
    public function verifyResendCode(Request $request)
    {
        $validation = Validator::make($request->all(), ['phone' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 400);
        }

        $user = User::where('phone', $validation['phone'])->first();

        if (!$user) {
            return response()->json(['invalid_data' => 'حدث خطأ'], 400);
        }

        //resend verification code
        $this->sendVerificationCode($user);

        return response()->json(['msg' => 'تم ارسال الكود']);
    }

    //show profile page
    public function profile(Request $request, $username = null)
    {
        $profile = is_null($username) ? Auth::user() : User::where('username', $username)->active()->first();

        abort_if(is_null($profile), 404);

        $profile->load('info');
        $profile->load('country');

        if ($profile->isArtistic()) {
            $profile->load('artistEvent');
        }

        if ($profile->isGallery()) {
            $profile->load('events');
        }

        return view('theme.profile', compact('profile'));
    }

    //update profile
    public function updateProfile(Request $request)
    {
        $profile = Auth::user();
        $logout = false;
        $refresh = false;

        //updated
        if ($request->name) $profile->name = $request->name;
        if ($request->birthday) $profile->birthday = $request->birthday;
        if ($request->website_url) $profile->website_url = $request->website_url;
        if ($request->country_id) $profile->country_id = $request->country_id;
        if ($request->city_id) $profile->city_id = $request->city_id;
        if ($request->area) $profile->area = $request->area;
        if ($request->postal_code) $profile->postal_code = $request->postal_code;
        if ($request->id_number) $profile->id_number = $request->id_number;
        if ($request->social_media) $profile->social_media = json_encode($request->social_media);

        //update username
        if ($request->username) {
            $not_unique = User::select('id', 'username')->where('username', $request->username)->where('id', '!=', $profile->id)->exists();
            if ($not_unique) {
                return \response()->json(['username' => 'الاسم الفريد يجب ان يكون غير مكرر'], 400);
            }
            $profile->username = $request->username;
            $refresh = true;
        }

        //update avatar
        if ($request->hasFile('avatar')) {
            $avatar_validate = Validator::make($request->all(), ['avatar' => 'required|image']);
            if ($avatar_validate->fails()) {
                return \response()->json($avatar_validate->errors(), 400);
            }
            $profile->avatar = $request->avatar->store('users');
            $refresh = true;
        }

        //update email
        if ($request->email) {
            $email_not_unique = User::select('id', 'email')->where('email', $request->email)->where('id', '!=', $profile->id)->exists();
            if ($email_not_unique) {
                return \response()->json(['email' => 'البريد الالكتروني يجب ان يكون غير مكرر'], 400);
            }
            $profile->email = $request->email;
            $refresh = true;
        }

        //update phone
        if ($request->phone) {
            //to update phone
            //password is required
            if (!$request->password) {
                return \response()->json(['password' => 'كلمة المرور مطلوبة'], 400);
            }
            //check password equal current password
            if (!Hash::check($request->password, $profile->password)) {
                return \response()->json(['password' => 'كلمة المرور غير متطابقة مع كلمة المرور الحالية'], 400);
            }
            //check phone unique number
            $phone_not_unique = User::select('id', 'phone')->where('phone', $request->phone)->where('id', '!=', $profile->id)->exists();
            if ($phone_not_unique) {
                return \response()->json(['phone' => 'رقم الهاتف يجب ان يكون غير مكرر'], 400);
            }

            $profile->phone = $request->phone;
            $profile->code = mt_rand(111111, 999999);
            $logout = true;
        }

        //update password
        if ($request->password) {
            //validate password
            $password_validate = Validator::make($request->all(), ['password' => '_password']);
            if ($password_validate->fails()) {
                return \response()->json($password_validate->errors(), 400);
            }

            //check password equal current password
            if (!Hash::check($request->password, $profile->password)) {
                return \response()->json(['password' => 'كلمة المرور غير متطابقة مع كلمة المرور الحالية'], 400);
            }

            $profile->password = Hash::make($request->password);
        }

        //update cv
        if ($request->hasFile('cv')) {
            $cv_validate = Validator::make($request->all(), ['cv' => 'mimes:pdf']);
            if ($cv_validate->fails()) {
                return \response()->json($cv_validate->errors(), 400);
            }
            $cv_path = $request->cv->store('cv');
            $original_name = $request->cv->getClientOriginalName() . '.' . $request->photo->extension();
            $json = [['download_link' => $cv_path, 'original_name' => $original_name]];
            $profile->cv = json_encode($json);
        }

        //save updates profile
        $profile->save();
        $msg = 'تم تعديل الملف الشخصي بنجاح';

        return \response()->json(compact('profile', 'logout', 'refresh', 'msg'));
    }

    //update user info [artist || gallery]
    public function updateInfo(Request $request)
    {
        $profile_info = Auth::user()->info;
        if ($request->bank_name) $profile_info->bank_name = $request->bank_name;
        if ($request->bank_account_number) $profile_info->bank_account_number = $request->bank_account_number;
        if ($request->bank_iban) $profile_info->bank_iban = $request->bank_iban;
        if ($request->international_number) $profile_info->international_number = $request->international_number;
        if ($request->display_name) $profile_info->display_name = $request->display_name;
        if ($request->qualification) $profile_info->qualification = $request->qualification;
        if ($request->years_experience) $profile_info->years_experience = $request->years_experience;
        if ($request->digital_signature) $profile_info->digital_signature = $request->digital_signature;
        if ($request->digital_signature_position_x) $profile_info->digital_signature_position_x = $request->digital_signature_position_x;
        if ($request->digital_signature_position_y) $profile_info->digital_signature_position_y = $request->digital_signature_position_y;
        $profile_info->save();

        $msg = 'تم تعديل الملف الشخصي بنجاح';

        return \response()->json(compact('profile_info', 'msg'));
    }

    //remove account
    public function removeAccount(Request $request)
    {
        $validate = Validator::make($request->all(), ['password' => 'required']);

        if ($validate->fails()) {
            return \response()->json($validate->errors(), 400);
        }

        $user = Auth::user();
        $password = $request->password;

        if (!Hash::check($password, $user->password)) {
            return \response()->json(['invalid_data' => 'كلمة السر غير صحيحة'], 400);
        }

        $user->delete();

        return redirect()->route('home');
    }

    //send verification code
    public function sendVerificationCode($user)
    {
        $phone = $user->getPhoneWithCode();
        $text = 'Verification Code : ' . $user->code;
        _sendSmsByNexmo($phone, $text);
    }

}

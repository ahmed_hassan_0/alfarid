<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ProductController extends VoyagerBaseController
{


    public function store(Request $request)
    {
        $request->request->add(['features' => $this->features($request)]);

        return parent::store($request);
    }

    public function update(Request $request, $id)
    {
        $request->request->add(['features' => $this->features($request)]);

//        dd($request->all());

        return parent::update($request, $id);
    }

    private function features(Request $request)
    {
        $ar = [];
        $en = [];

        if ($request->filled('features') && is_array($request->features)) {
            $ar = !isset($request->features['ar']) ? [] : $request->features['ar'];
            $en = !isset($request->features['en']) ? [] : $request->features['en'];
        }

        $features = json_encode(['ar' => $ar, 'en' => $en]);

        return $features;
    }

}

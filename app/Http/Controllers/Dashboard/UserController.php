<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Roles;
use App\Helpers\Utilities;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Role;

class UserController extends OverrideUserController
{


    public function store(Request $request)
    {
        $slugFake = $this->getSlug($request);

        $request->validate(Utilities::userRoles($slugFake));

        $slug = 'admins';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $role = Roles::User;

        $request->request->add(['role_id' => Role::where('name', $role)->value('id')]);

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            return redirect()
                ->route("voyager.{$slugFake}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                    'alert-type' => 'success',
                ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }


    public function update(Request $request, $id)
    {
        $slugFake = $this->getSlug($request);

        $request->validate(Utilities::userRoles($slugFake, $id));

        $slug = 'admins';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $role = Roles::User;

        $request->request->add(['role_id' => Role::where('name', $role)->value('id')]);

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        return redirect()
            ->route("voyager.{$slugFake}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
    }
}

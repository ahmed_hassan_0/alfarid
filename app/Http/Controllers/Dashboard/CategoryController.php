<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class CategoryController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $categoryNextOrder = (((int)Category::orderBy('order', 'desc')->limit(1)->value('order')) + 1);
        $request->request->add(['order' => $categoryNextOrder]);
        return parent::store($request);
    }
}

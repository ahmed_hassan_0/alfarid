<?php

namespace App\Http\Controllers\Dashboard\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    public function showLinkRequestForm()
    {
        return view('vendor.voyager.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );


//        if ($response == 'passwords.sent') {
//            $tokenRow = DB::table('password_resets')->where('email', $request->email)->latest()->first();
//            if ($tokenRow) {
//                $token = $tokenRow->token;
//                $message = '<a style="padding: 10px 20px; color: #fff; background-color: #22A7F0" href="'. route('password.reset') .'?email='. $request->email .'&token='. $token .'">';
//                $message .= 'اضغط لتعيين كلمة المرور الخاصه بك';
//                $message .= '</a>';
//                _sendEmail(trim($request->email), 'اعادة تعيين كلمة المرور', $message);
//            }
//        }

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

}
